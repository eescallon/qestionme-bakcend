<?php
namespace Qestion\QestionBundle\Controller;

use Qestion\QestionBundle\Entity\Game;
use Qestion\QestionBundle\Entity\Player;
use Qestion\QestionBundle\Entity\User;
use Qestion\QestionBundle\Entity\QuestionGame;

/**
* clase PlayerController
*
* Esta clase contiene atributos y funciones referentes a Roles
*
* @package    Qestion
* @subpackage QestionBundle
* @author     Eduardo Escallon. < eduard.escallon@gmail.com >
*/
class PlayerController extends Controller {

	/**
     * Función que obtiene y retorna a traves de un String, el nombre del Controlador actual.
     *
     * @return string 
     */
    public function getName()
    {
        return "Player";
    }

    public function selectRandomPlayersAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	$dm = $this->get('doctrine_mongodb')->getManager();
        $nodeService = $this->get('nodeService');
    	$normalizer = $this->get("talker")->getNormalizer();
    	$gameRepo = $em->getRepository("QestionBundle:Game");
    	$game = $gameRepo->getGameNotStarted();
    	if($game['total'] == 0)
    	{
    		$g = $this->createNewGame("prueba", 20);
	        $gameNorm = $normalizer->normalize($g, "Qestion\QestionBundle\Entity\Game");
    		return $this->get("talker")->response(array("total" => 1, "data" => $gameNorm));
    	}
    	else{
    		$g = $game['data'];
    		$user = $this->get('security.context')->getToken()->getUser();
            $found = false;
            $start = false;
            foreach($g->getPlayers() as $player)
            {
                if($player->getPlayer()->getId() == $user->getId())
                {
                    $found = true;
                    break;
                }
            }
            if(!$found){
                $newPlayer = $this->insertPlayerInGame($g, $user);
                $playerNorm = $normalizer->normalize($newPlayer, "Qestion\QestionBundle\Entity\Player");
                if(count($g->getPlayers()) == 4)
                {
                    $start = true;
                }
                $nodeService->sendMessage("broadcast", array(
                    "message" => "newPlayer".$g->getId(),
                    "content" => array(
                        "player" => $playerNorm,
                    )
                ));
            }

    		$gameNorm = $normalizer->normalize($g, "Qestion\QestionBundle\Entity\Game");
    		return $this->get("talker")->response(array("total" => 1, "data" => $gameNorm, "start" => $start));
    	}
    }

    private function formatQuestions($questions)
    {
    	$arrQuestions = array();
    	foreach($questions as $key => $value)
    	{
    		$arrayAss = array();
    		foreach($value['answers'] as $answer)
    		{
    			$arrayAss[] = array(
					'id' => $value['id'],
					'answer' => $value['answer'],
					'oa' => $value['selected'],
					'help' => $value['help']
				);
    		}
    		$questions[$key]['answers'] = $arrayAss;
    	}
    }

    private function createNewGame($name, $totalQuestions)
    {
    	$em = $this->getDoctrine()->getManager();
    	$game = new Game();
    	$game->setName($name);
    	$game->setTotalQuestions($totalQuestions);
    	$questionManager = $this->get('questionManager');
    	$questions = $questionManager->getQuestions($totalQuestions);
    	$this->setQuestionsInGame($game, $questions);
		$user = $this->get('security.context')->getToken()->getUser();
		$this->insertPlayerInGame($game, $user);

    	$em->persist($game);
    	$em->flush();
    	return $game;
    }

    private function setQuestionsInGame($game, $questions)
    {
    	$em = $this->getDoctrine()->getManager();
    	foreach($questions as $question)
    	{
    		$questionGame = new QuestionGame();
    		$questionGame->setGame($game);
    		$questionGame->setQuestion($question);
    		$em->persist($questionGame);
    	}
    }

    private function insertPlayerInGame(&$game, $user)
    {
        $em = $this->getDoctrine()->getManager();
    	$player = new Player();
    	$player->setPlayer($user);
        $player->setGame($game);
    	$em->persist($player);
        $game->addPlayer($player);
        $em->flush();
        return $player;
    }

    public function updatePasswordAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->get("request");
        $data = $request->getContent();
        $data = json_decode($data, true);
        $oldPassword = $data['oldPassword'];
        $newPassword = $data['newPassword'];
 
        $user = $this->container->get('security.context')->getToken()->getUser();
        
        $oldPassword = $this->generatePassword($oldPassword);
        $newPassword = $this->generatePassword($newPassword);

        if($oldPassword != $user->getPassword()){
            throw new \Exception("Wrong Password");            
        }

        $this->changePassword($newPassword);

        return $this->get("talker")->response($this->getAnswer(true, $this->update_successful));
    }

    private function changePassword($password)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $user = $this->container->get('security.context')->getToken()->getUser();
        $user->setPassword($password);

        $em->flush();
    }

    /**
     * Codifica el password usando el Salt del usuario
     * 
     * @param password - Password a codificar
     * @return password - Paswword codificado
     */
    private function generatePassword($password)
    {
        $user = new User();
        $factory = $this->container->get('security.encoder_factory');
        $codificador = $factory->getEncoder($user);
        $password = $codificador->encodePassword($password, $user->getSalt());
        return $password;
    }
}
?>