<?php
namespace Qestion\QestionBundle\Controller;

use Qestion\QestionBundle\Document\TurnQuestion;

/**
* clase AnswerController
*
* Esta clase contiene atributos y funciones referentes a Roles
*
* @package    Qestion
* @subpackage QestionBundle
* @author     Eduardo Escallon. < eduard.escallon@gmail.com >
*/
class AnswerController extends Controller {

	/**
     * Función que obtiene y retorna a traves de un String, el nombre del Controlador actual.
     * este es un comentario de prueba
     * @return string
     */
    public function getName()
    {
        return "Answer";
    }

    public function checkAnswerAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	$dm = $this->get('doctrine_mongodb')->getManager();
        $nodeService = $this->get('nodeService');
        $request = $this->get("request");
        $data = $request->getContent();
        $data = json_decode($data, true);
        $gameId = $data["gameId"];
        $answerId = $data["answerId"];
        $questionId = $data["questionId"];
        $normalizer = $this->get("talker")->getNormalizer();
		$user = $this->get('security.context')->getToken()->getUser();
    	$questionRepo = $em->getRepository("QestionBundle:Question");
    	$qestionGameRepo = $em->getRepository("QestionBundle:QuestionGame");
    	$turnQuestionRepo = $dm->getRepository("QestionBundle:TurnQuestion");
    	$gameRepo = $em->getRepository("QestionBundle:Game");
    	$game = $gameRepo->find($gameId);
    	$players = $game->getPlayers();
    	$totalPlayers = count($players);
    	$question = $questionRepo->find($questionId);
    	$right = false;
        $noAnswer = false;
        if($answerId != "0")
        {
            foreach($question->getAnswers() as $answer)
            {
                if($answer->getId() == $answerId)
                {
                    if($answer->getSelected())
                    {
                        $right = true;
                    }
                    break;
                }
            }
        }
        else{
            $noAnswer = true;
        }
   		$playerAnswering = $game->getPlayerAnswering();
    	$this->logAnswer($questionId, $gameId, $playerAnswering->getId(), $answerId, $right);
    	$newPlayerTurn = null;
    	$newQuestion = null;
    	$finish = false;
        $rank = null;
    	$newPointsPlayer = $playerAnswering->getPoints();
    	$playerTurn = $game->getPlayerTurn();
    	if($right)
    	{
    		$playerAnswering->setPoints($playerAnswering->getPoints() + 1);
    		$newPointsPlayer++;
    		
    		$newPosition = null;
    		if($playerTurn->getPosition() == $totalPlayers)
    		{
    			$newPosition = 1;
    		}
    		else{
    			$newPosition = $playerTurn->getPosition() + 1;
    		}
    		foreach($players as $player)
			{
				if($player->getPosition() == $newPosition)
				{
					$newPlayerTurn = $player;
					break;
				}
			}
			$game->setPlayerTurn($newPlayerTurn);
			$game->setPlayerAnswering($newPlayerTurn);
			$game->setAskedQuestion($game->getAskedQuestion()+1);
			$newQuestionData = $qestionGameRepo->findOneBy(array("game" => $game, "checked" => false));
			if($newQuestionData){
				$newQuestionData->setChecked(true);
				$newQuestion = $newQuestionData->getQuestion();
			}
			else{
				$finish = true;
                $rank = $this->getRanking($game);
			}
	        
    	}
    	else{
            // echo $gameId." - ".$questionId."\n";
    		$logAnswers = $turnQuestionRepo->findBy(array("gameId" => strval($gameId), "questionId" => strval($questionId)));
            // print_r($logAnswers);
    		$totalLog = count($logAnswers);
    		if($totalLog == $totalPlayers)
    		{
    			$newPosition = null;
	    		if($playerTurn->getPosition() == $totalPlayers)
	    		{
	    			$newPosition = 1;
	    		}
	    		else{
	    			$newPosition = $playerTurn->getPosition() + 1;
	    		}
	    		foreach($players as $player)
				{
					if($player->getPosition() == $newPosition)
					{
						$newPlayerTurn = $player;
						break;
					}
				}
				$game->setPlayerTurn($newPlayerTurn);
				$game->setPlayerAnswering($newPlayerTurn);
				$game->setAskedQuestion($game->getAskedQuestion()+1);
				$newQuestionData = $qestionGameRepo->findOneBy(array("game" => $game, "checked" => false));
				if($newQuestionData){
					$newQuestionData->setChecked(true);
		        	$newQuestion = $newQuestionData->getQuestion();
		        }
		        else{
		        	$finish = true;
                    $rank = $this->getRanking($game);
		        }
    		}
    		else{
    			$newPosition = null;
	    		if($playerAnswering->getPosition() == $totalPlayers)
	    		{
	    			$newPosition = 1;
	    		}
	    		else{
	    			$newPosition = $playerAnswering->getPosition() + 1;
	    		}
	    		foreach($players as $player)
				{
					if($player->getPosition() == $newPosition)
					{
						$newPlayerTurn = $player;
						break;
					}
				}
				$game->setPlayerAnswering($newPlayerTurn);
    		}
    	}
    	$playerTurn = null;
    	$questionNorm = null;
    	if(!is_null($newPlayerTurn))
    	{
        	$playerTurn = $normalizer->normalize($newPlayerTurn, "Qestion\QestionBundle\Entity\Player");
    	}
    	if(!is_null($newQuestion))
    	{
	        $questionNorm = $normalizer->normalize($newQuestion, "Qestion\QestionBundle\Entity\Question");
	        foreach($questionNorm['answers'] as $key => $answer)
        	{
	            unset($questionNorm['answers'][$key]['selected']);
	        }
    	}
    	$userPlayed = $normalizer->normalize($user, "Qestion\QestionBundle\Entity\Player");
    	$em->flush();
    	$nodeService->sendMessage("broadcast", array(
            "message" => "checkanswer".$gameId,
            "content" => array(
                "noAnswer" => $noAnswer,
                "success" => $right,
                "message" => "Pregunta Checkeada con exito",
                "question" => $questionNorm,
                "newPoints" => $newPointsPlayer,
                "turn" => $playerTurn,
                "played" => $userPlayed,
                "finish" => $finish,
                "rank" => $rank
            )
        ));
    	return $this->get('talker')->response(array("success" => true, "message" => "Respuesta verificada"));
    }

    private function getRanking($game)
    {
        $normalizer = $this->get("talker")->getNormalizer();
        $playersArray = array();
        foreach($game->getPlayers() as $player)
        {
            $playersArray[] = $normalizer->normalize($player, "Qestion\QestionBundle\Entity\Player");
        }
        $order = array();
        foreach($playersArray as $key => $value)
        {
            $order[$key] = $value['points'];
        }
        array_multisort($order, SORT_DESC, $playersArray);
        return $playersArray;
    }

    private function logAnswer($questionId, $gameId, $playerId, $answerId, $right)
    {
    	$dm = $this->get('doctrine_mongodb')->getManager();
    	$turnQuestion = new TurnQuestion();
    	$turnQuestion->setGameId($gameId);
    	$turnQuestion->setPlayerId($playerId);
    	$turnQuestion->setQuestionId($questionId);
    	$turnQuestion->setAnswerId($answerId);
    	$turnQuestion->setRight($right);
    	$dm->persist($turnQuestion);
    	$dm->flush();
    }
}
?>
