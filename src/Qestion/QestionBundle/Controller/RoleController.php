<?php
namespace Qestion\QestionBundle\Controller;

/**
* clase RoleController
*
* Esta clase contiene atributos y funciones referentes a Roles
*
* @package    Qestion
* @subpackage QestionBundle
* @author     Eduardo Escallon. < eduard.escallon@gmail.com >
*/
class RoleController extends Controller {

	/**
     * Función que obtiene y retorna a traves de un String, el nombre del Controlador actual.
     *
     * @return string 
     */
    public function getName()
    {
        return "Role";
    }

    public function listAction($id = null)
    {
    	$em = $this->getDoctrine()->getEntityManager();
		$request = $this->get("request");
		$className = $this->namespace.$this->getName();
		$md = $em->getClassMetadata($className);
		$associations = $md->associationMappings;
		//print_r($associations);
		$fieldMapping = $md->fieldMappings;		
		if(is_null($id)){
			//echo "entro";
			$criteria = array();
			foreach($associations as $name => $value){
				$parameter = $request->get($name);
				if($parameter)
				{
					$criteria[$name] = $parameter;
				}
			}
		
			foreach($fieldMapping as $name => $value){
				$parameter = $request->get($name);
				if($parameter)
				{
					$criteria[$name] = $parameter;
				}
			}
			
			$user = $this->get('security.context')->getToken()->getUser();

			$roles = $user->getRoles();
			$role = $roles[0];
			$filter = $request->get('filter');
			if($filter){
				$filter = json_decode($filter, true);
			}
			
			$page = $request->get("page");
			$start = $request->get("start");
			$limit = $request->get("limit");			
	
			$bundleName = $this->getBundleName();
			$repo = $em->getRepository($bundleName.":" . $this->getName());
			$list = $repo->findRolesWithPermission(null,$filter);

			$this->afterList($list);
			
			return $this->get("talker")->response(array("total" => count($list), "data" => $list));
			
		}else{
			//echo "entro 2";
			$idProperty = $md->identifier;
			$idProperty = $idProperty[0];
			$idType = $fieldMapping[$idProperty]['type'];
			$val = $this->validateType($idType, $id);
			if($val)
			{
				$bundleName = $this->getBundleName();
				$repo = $em->getRepository($bundleName.":" . $this->getName());
				$list = $repo->findRolesWithPermission($id);
				if(count($list)>0) $list = $list[0];
				else throw $this->createNotFoundException('El recurso no existe.');
				$this->afterList($list);
				return $this->get("talker")->response($list);
			}
			else{
				throw $this->createNotFoundException('El recurso no existe.');
			}
			
		}
    }
}
?>