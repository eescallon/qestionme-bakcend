<?php
namespace Qestion\QestionBundle\Controller;

/**
* clase CategoryController
*
* Esta clase contiene atributos y funciones referentes a Roles
*
* @package    Qestion
* @subpackage QestionBundle
* @author     Eduardo Escallon. < eduard.escallon@gmail.com >
*/
class CategoryController extends Controller {

	/**
     * Función que obtiene y retorna a traves de un String, el nombre del Controlador actual.
     *
     * @return string 
     */
    public function getName()
    {
        return "Category";
    }
}
?>