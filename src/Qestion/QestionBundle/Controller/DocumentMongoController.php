<?php
namespace Qestion\QestionBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
abstract class DocumentMongoController extends Controller
{
    public $namespace = "Qestion\QestionBundle\Document\\";
    public $bundleName = "QestionBundle";
    
    protected function normalizer($entity, $class){
        
        $dm = $this->get('doctrine_mongodb')->getManager();
        $md = $dm->getClassMetadata($this->namespace.$this->getName());
        // print_r($md);
        $fm = $md->fieldMappings;
        $arr = array();
        foreach($fm as $key => $value)
        {
            $arr[$key] = $entity->{'get'.ucfirst($key)}();
        }
        return $arr;
    }
    
    protected function encode($array){
        $type = $this->get("talker")->getType();
        $value = $this->get("talker")->getSerialize()->encode($array, $type['type']);
        return $value;
    }
    
    protected function serialize($entity){
        $arr = $this->normalizer($entity, $this->namespace.$this->getName());
        return $this->encode($arr);
    }
    
    protected function ownResponse($data){
        $response = $this->get("talker")->getResponse();
        $response->setContent($data);
        return $response;
    }
    /**
     * Funcion que revisa por que metodo llega la ruta de comentarios de una entidad
     */
    public function resolveRoute()
    {
        $request = $this->get("request");
        $method = $request->getMethod();
        switch($method)
        {
            case 'GET':
                return $this->listAction();
                break;
            case 'POST':                
                return $this->saveAction();
                break;
            case 'PUT':
                return $this->updateAction();
                break;
            case 'DELETE':
                return $this->deleteAction();
                break;
        }
    }
    public function listAction($id = null)
    {
        $dm = $this->get('doctrine_mongodb')->getManager(); 
        $request = $this->get("request");
        $normalizer = $this->get("talker")->getNormalizer();
        $repo = $dm->getRepository($this->getBundleName().":".$this->getName());
        if(is_null($id))
        {
            $skip = $request->get("skip");
            $pageSize = $request->get("pageSize");
            $filter = $request->get('filter');
            if($filter)
            {
                $filter = json_decode($filter, true);
            }
            
           
            $document = $repo->consultDocument($dm, $this->getName(), null ,null, $filter, $this->getBundleName(), $skip, $pageSize);
// 			print_r($document);
            $ret=array();
            foreach($document['data'] as $id => $obj){
                $entity = $normalizer->normalizerMongo($obj, $this->namespace.$this->getName());
                $ret[] = $entity;
            }
            $return = array(
                'total' => $document['total'],
                'data' => $ret
            );
            $this->afterList($return);
            
            return $this->ownResponse($this->encode($return));
        }
        else{
            $document = $repo->consultDocument($dm, $this->getName(), $id);
            $entity = $normalizer->normalizerMongo($document[$id], $this->namespace.$this->getName());
            $this->afterList($entity);
            return $this->ownResponse($this->encode($entity));
        }   
    }
    // +++++++++++++++++++ SAVE +++++++++++++++++++++++++++++
    public function saveAction()
    {
        $documentService = $this->get("documentController");
        $normalizer = $this->get("talker")->getNormalizer();
        $className = $this->namespace.$this->getName();
        
        $document = $documentService->getRequestDocument($className, null, true);
        $this->beforeSave($document);
        // $documentService->checkBoolean($document, $className);
        $save = $documentService->doSave($document);
        $this->afterSave($document);
        $this->entityPost = $document;
        $normalizer->setInfoToLog($className, $this->entityPost);
        $ids = array();
        if(is_array($document))
        {
            foreach($document as $ent)
            {
                // $this->addParticipant($ent);
                $ids[] = $ent->getId();
            }
        }
        else{
            // $this->addParticipant($document);
            $ids[] = $document->getId();
        }
        // echo $document->getQuantity();
        return $this->get("talker")->response(array("success" => true, "message" => $this->save_successful, "total" => count($ids), "data" => $ids), 201);
    }
    // +++++++++++++++++++ UPDATE +++++++++++++++++++++++++++++
    public function updateAction($id = null)
    {
        $dm = $this->get('doctrine_mongodb')->getManager();
        $className = $this->namespace.$this->getName();
        $documentService = $this->get("documentController");
        $normalizer = $this->get("talker")->getNormalizer();
        $md = $dm->getClassMetadata($className);
        $associations = $md->associationMappings;
        $fieldMapping = $md->fieldMappings;
        $idProperty = $md->identifier;
        $idType = $fieldMapping[$idProperty]['type'];
        $ec = $documentService->getRequestDocument($className, $id);
        $this->beforeUpdate($ec);
        $doUpdate = $documentService->doUpdate($ec);
        $this->afterUpdate($doUpdate);
        $normalizer->setInfoToLog($className, $this->entityPost);
        return $this->get("talker")->response($this->getAnswer(true, $this->update_successful));
    }
    public function deleteAction($id = null)
    {
        $documentService = $this->get("documentController");
        $normalizer = $this->get("talker")->getNormalizer();
        $className = $this->namespace.$this->getName();
        if(!is_null($id)){
            $bundleName = $this->getBundleName();
            $dm = $this->get('doctrine_mongodb')->getManager();
            $repo = $dm->getRepository($bundleName.":" . $this->getName());
            $md = $dm->getClassMetadata($className);
            $associations = $md->associationMappings;
            $fieldMapping = $md->fieldMappings;
            $idProperty = $md->identifier;
            $idType = $fieldMapping[$idProperty]['type'];
            $ec = $repo->findOneBy(array("id" => $id, "deleted" => false));
            if(!$ec)
            {
                throw $this->createNotFoundException('Entity Not Found.');
            }
        }else{
            $ec = $documentService->getRequestEntity($className);
        }
        $this->beforeDelete($ec);
        $this->entityDeleted = $ec;
        $doDelete = $documentService->doDelete($ec);
        $this->afterDelete($doDelete);
        $normalizer->setInfoToLog($className, $this->entityPost, $this->entityDeleted);
        return $this->get("talker")->response($this->getAnswer(true, $this->delete_successful), 204);
    }
    protected function afterList(&$Document){}
}