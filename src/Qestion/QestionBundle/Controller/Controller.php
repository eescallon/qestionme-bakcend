<?php
namespace Qestion\QestionBundle\Controller;
use Symfony\Component\Process\Exception\RuntimeException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller as SymfonyController;

/**
* Controller
*
* Esta clase contiene atributos y funciones generales eferentes a Entidades
*
* @package    Dialboxes
* @subpackage AppSecurityBundle
* @author     Ing. Deiner Mejia M. < deiner37@gmail.com >
*/
abstract class Controller extends SymfonyController {
	
	public $namespace = "Qestion\QestionBundle\Entity\\";
	
	protected $save_successful = "Save Successful";
	
	protected $update_successful = "Update Successful";
	
	protected $delete_successful ="Delete Successful";
	protected $entityDeleted = null;
    protected $entityPost = null;
		
	public function getAnswer($success, $message) {
		return array (
			"success" => $success,
			"message" => $message
		);
	}
	public function listEntitiesFirstLevelAction(){
		$em = $this->getDoctrine()->getManager();
		$request = $this->get('request');
		$className = $this->namespace.$this->getName();
		$filter = $request->get('filter');
		if($filter){
			$filter = json_decode($filter, true);
		}
		$relations = $request->get('relations');
		if($relations){
			$relations = json_decode($relations, true);
		}
		$skip = $request->get("skip");
		$pageSize = $request->get('pageSize');
		$limit = $skip + $pageSize;
		$sort = $request->get('sort');
		$orderType = $request->get('orderType');
		$bundleName = $this->getBundleName();
		$repo = $em->getRepository($bundleName.":" . $this->getName());	
		$list = $repo->getEntity(null, $sort, $filter, $skip, $pageSize, $relations, $orderType);
		$this->afterListZero($list);
		return $this->get("talker")->response($list);
	}
	
	public function listAction($id = null) {
		$em = $this->getDoctrine()->getManager();
		$request = $this->get("request");
		$className = $this->namespace.$this->getName();
		$md = $em->getClassMetadata($className);
		$associations = $md->associationMappings;
		$fieldMapping = $md->fieldMappings;
		if(is_null($id)){
			//echo "entro";
			$criteria = array();
			foreach($associations as $name => $value){
				$parameter = $request->get($name);
				if($parameter)
				{
					$criteria[$name] = $parameter;
				}
			}
		
			foreach($fieldMapping as $name => $value){
				$parameter = $request->get($name);
				if($parameter)
				{
					$criteria[$name] = $parameter;
				}
			}
			
			$user = $this->get('security.context')->getToken()->getUser();
			
			$filter = $request->get('filter');
			if($filter){
				$filter = json_decode($filter, true);
			}
			$skip = $request->get("skip");
			$page = $request->get('page');
			$pageSize = $request->get('pageSize');
			$limit = $skip + $pageSize;
			$sort = $request->get('sort');
			$orderType = $request->get('orderType');
	
			$bundleName = $this->getBundleName();
			$repo = $em->getRepository($bundleName.":" . $this->getName());	
			$list = $repo->getArrayEntityWithOneLevel($criteria, $sort, $skip, $pageSize, $filter, $orderType);
			$this->afterList($list);
			
			return $this->get("talker")->response($list);
			
		}else{
			// echo "entro 2";
			$idProperty = $md->identifier;
			$idProperty = $idProperty[0];
			$idType = $fieldMapping[$idProperty]['type'];
			// print_r($fieldMapping);
			// echo $idType." - ".$id;
			$val = $this->validateType($idType, $id);
			if($val)
			{
				// echo "entre";
				$bundleName = $this->getBundleName();
				//echo $bundleName.":" . $this->getName();
				$repo = $em->getRepository($bundleName.":" . $this->getName());
				$list = $repo->getArrayEntityWithOneLevel(array("id" => $id));
				// print_r($list);
				//$list = $repo->getArrayBy(array("id" => $id));
				$list = $list["data"];
				if(count($list)>0) $list = $list[0];
				else throw $this->createNotFoundException('El recurso no existe.');
				$this->afterList($list);
				return $this->get("talker")->response($list);
			}else{
				throw $this->createNotFoundException('El recurso no existe.');
			}
			
		}
	}
	protected function validateType($type, $value)
	{
		// echo $type;
		// echo "------------".$value;
		switch ($type) {
			case 'uuid':
				if(preg_match('/([a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12})/', $value, $matches)){
					// echo "aqui";
					return true;
				}
				else{
					// echo "aqui2";
					return false;
				}
				break;
			case 'integer':
				if(is_numeric($value))
				{
					return true;
				}
				else{
					return false;
				}
			
			default:
				# code...
				break;
		}
		
	}
	
	protected function getBundleName(){
		$request = $this->get("request");
		$controller = $request->attributes->get('_controller');
		$matches    = explode("\\", $controller);
		if(count($matches) > 1)
			return  $matches[1];
		else{
			$matches = explode(":", $controller);
			return  $matches[0];
		}
	}
	
	public function resolveRouteAction(Request $request)
	{
		$method = $request->getMethod();
		switch($method)
		{
			case 'GET':
				return $this->listAction();
				break;
			case 'POST':
				$contentType = $request->headers->get('content_type');
		        $explode = explode(";", $contentType);
		        $contentType = $explode[0];
		 		$action = $request->headers->get('X-ACCION');
				if($action){
		         	if($contentType == 'multipart/form-data'){
			        	$id = $request->get('id');
			        	if($action == 'update'){
							return $this->updateAction($id);
			        	}
			        }
		        }else{
		        	return $this->saveAction();
		        }
				break;
			case 'PUT':
				// echo "Entro controller";
				return $this->updateAction();
				break;
			case "DELETE":
				return $this->deleteAction();
				break;
		}
	}
	
	// +++++++++++++++++++ SAVE +++++++++++++++++++++++++++++
	public function saveAction() {
		$entityService = $this->get("entityController");
		$normalizer = $this->get("talker")->getNormalizer();
    	$className = $this->namespace.$this->getName();
		$entity = $entityService->getRequestEntity($className);
		$responseBeforeSave = $this->beforeSave($entity);
		$save = $entityService->doSave($entity);
		$this->afterSave($entity);
		$this->entityPost = $entity;
		$ids = array();
		if(is_array($entity))
		{
			foreach($entity as $ent)
			{
				$normalizer->setInfoToLog($className, $ent);
				// $this->addParticipant($ent);
				$ids[] = $ent->getId();
			}
		}
		else{
			$normalizer->setInfoToLog($className, $this->entityPost);
			// $this->addParticipant($entity);
			$ids[] = $entity->getId();
		}
		return $this->get("talker")->response(array("success" => true, "message" => $this->save_successful, "total" => count($ids), "data" => $ids, 'beforeSave' => $responseBeforeSave), 201);
	}
	
	// +++++++++++++++++++ UPDATE +++++++++++++++++++++++++++++
	public function updateAction($id = null) {
		
		$em = $this->getDoctrine()->getEntityManager();
		
		$className = $this->namespace.$this->getName();
		$entityService = $this->get("entityController");
		$normalizer = $this->get("talker")->getNormalizer();
		$md = $em->getClassMetadata($className);
		$associations = $md->associationMappings;
		$fieldMapping = $md->fieldMappings;
		$idProperty = $md->identifier;
		$idProperty = $idProperty[0];
		$idType = $fieldMapping[$idProperty]['type'];
		$val = $this->validateType($idType, $id);
		if($val)
		{
			$ec = $entityService->getRequestEntity($className, $id);
		}
		else{
			throw $this->createNotFoundException('Entity Not Found.');
		}
		$this->beforeUpdate($ec);
		
		$doUpdate = $entityService->doUpdate($ec);
		$this->afterUpdate($doUpdate);
		$entityService->setInfoToLog($className, $this->entityPost);
		$normalizer->setInfoToLog($className, $this->entityPost);
		
		return $this->get("talker")->response($this->getAnswer(true, $this->update_successful));
	}
	
	// +++++++++++++++++++ DELETE +++++++++++++++++++++++++++++
	public function deleteAction($id = null) {
		$entityService = $this->get("entityController");
		$normalizer = $this->get("talker")->getNormalizer();
		$className = $this->namespace.$this->getName();
		if(!is_null($id)){
			$bundleName = $this->getBundleName();
			$em = $this->getDoctrine()->getEntityManager();
			$repo = $em->getRepository($bundleName.":" . $this->getName());
			$md = $em->getClassMetadata($className);
			$associations = $md->associationMappings;
			$fieldMapping = $md->fieldMappings;
			$idProperty = $md->identifier;
			$idProperty = $idProperty[0];
			$idType = $fieldMapping[$idProperty]['type'];
			$val = $this->validateType($idType, $id);
			if($val){
				$ec = $repo->findOneBy(array("id" => $id, "deleted" => false));
				if(!$ec)
				{
					throw $this->createNotFoundException('Entity Not Found.');
				}
			}
			else
				throw $this->createNotFoundException('Entity Not Found.');
		}else{
			$ec = $entityService->getRequestEntity($className);
		}
		$this->beforeDelete($ec);
		$this->entityDeleted = $ec;
		$doDelete = $entityService->doDelete($ec);
		$this->afterDelete($ec);
		$normalizer->setInfoToLog($className, $this->entityPost, $this->entityDeleted);
		return $this->get("talker")->response($this->getAnswer(true, $this->delete_successful), 204);
	}

	/**
	 * Funcin que realiza alguna accion despues de listar una entidad.
	 */
	protected function afterList(&$Entity) {
	}
	/**
	 * Funcin que realiza alguna accion despues de listar una entidad.
	 */
	protected function afterListZero(&$Entity) {
	}
	
	/**
	 * Funcin que realiza alguna accion antes de guardar una entidad.
	 */
	protected function beforeSave(&$Entity) {
	}
	
	/**
	 * Funcin que realiza alguna accion despues de guardar una entidad.
	 */
	protected function afterSave(&$Entity) {
	}
	
	protected function afterPersist(&$Entity) {
	}
	
	/**
	 * Funcin que realiza alguna accion antes de actualizar una entidad.
	 */
	protected function beforeUpdate(&$Entity) {
	}
	
	/**
	 * Funcin que realiza alguna accion despues de actualizar una entidad.
	 */
	protected function afterUpdate(&$Entity) {
	}
	
	/**
	 * Funcin que realiza alguna accion antes de eliminar una entidad.
	 */
	protected function beforeDelete(&$Entity) {
	}
	
	/**
	 * Funcin que realiza alguna accion despues de eliminar una entidad.
	 */
	protected function afterDelete(&$Entity) {}
	
	abstract protected function getName();
	
}