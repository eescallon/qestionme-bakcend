<?php
namespace Qestion\QestionBundle\Controller;

/**
* clase GameController
*
* Esta clase contiene atributos y funciones referentes a Roles
*
* @package    Qestion
* @subpackage QestionBundle
* @author     Eduardo Escallon. < eduard.escallon@gmail.com >
*/
class GameController extends Controller {

	/**
     * Función que obtiene y retorna a traves de un String, el nombre del Controlador actual.
     *
     * @return string
     */
    public function getName()
    {
        return "Game";
    }

    public function startGameAction($gameId)
    {
    	$em = $this->getDoctrine()->getManager();
        $gameRepo = $em->getRepository("QestionBundle:Game");
    	$qestionGameRepo = $em->getRepository("QestionBundle:QuestionGame");
        $normalizer = $this->get("talker")->getNormalizer();
        $nodeService = $this->get('nodeService');
    	$game = $gameRepo->find($gameId);
    	$players = $game->getPlayers();
        $turn = null;
    	foreach($players as $player)
    	{
    		if($player->getPosition() == 1)
    		{
    			$game->setPlayerTurn($player);
    			$game->setPlayerAnswering($player);
                $game->setState('I');
                $em->flush();
                $turn = $player;
    			break;
    		}
    	}
        $question = $qestionGameRepo->findOneBy(array("game" => $game, "checked" => false));
        $question->setChecked(true);
        $game->setAskedQuestion(1);
        $em->flush();
        $question = $question->getQuestion();
        $questionNorm = $normalizer->normalize($question, "Qestion\QestionBundle\Entity\Question");
        $playerTurn = $normalizer->normalize($turn, "Qestion\QestionBundle\Entity\Player");
        foreach($questionNorm['answers'] as $key => $answer)
        {
            unset($questionNorm['answers'][$key]['selected']);
        }
        $nodeService->sendMessage("broadcast", array(
            "message" => "startGame".$game->getId(),
            "content" => array(
                "success" => true,
                "message" => "Partida Iniciada con exito",
                "question" => $questionNorm,
                "turn" => $playerTurn
            )
        ));
    	return $this->get('talker')->response(array("success" => true, "message" => "Partida Iniciada con exito"));
    }
}
?>
