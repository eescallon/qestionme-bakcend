<?php
namespace Qestion\QestionBundle\Controller;

use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Qestion\QestionBundle\Entity\User;
use Qestion\QestionBundle\Document\Image;

/**
* clase UserController
*
* Esta clase contiene atributos y funciones eferentes a Usuarios
*
* @package    QestionMe
* @subpackage QestionBundle
* @author     Eduardo Escallon. < eduard.escallon@gmail.com >
*/
class UserController extends Controller {

 	static $URL_TOKEN = 'https://www.googleapis.com/oauth2/v2/userinfo';
	
    /**
     * Verifica si hay un error en la autenticacion del login
     */
	public function loginErrorAction(){
		$request = $this->get('request');
			if ($request->attributes
					->has(SecurityContext::AUTHENTICATION_ERROR)) {
				throw new Exception($request->getSession()->get(SecurityContext::AUTHENTICATION_ERROR), 401);
			} else {
				throw new Exception($request->getSession()->get(SecurityContext::AUTHENTICATION_ERROR), 401);
			}
			return $this->get("talker")->response($this->getAnswer(false, $error->getMessage()));
	}

	/**
     * Función que obtiene y retorna a traves de un String, el nombre del Controlador actual.
     *
     * @return string 
     */
    public function getName()
    {
        return "User";
    }

    /**
     * Codifica el password usando el Salt del usuario
     * 
     * @param password - Password a codificar
     * @return password - Paswword codificado
     */
    private function generatePassword($password)
    {
        $user = new User();
        $factory = $this->container->get('security.encoder_factory');
        $codificador = $factory->getEncoder($user);
        $password = $codificador->encodePassword($password, $user->getSalt());
        return $password;
    }

    private function getUserInfo($token)  {
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, self::$URL_TOKEN);
        curl_setopt($ch,CURLOPT_POST, 0);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "authorization: Bearer $token",
        ));
            
        $data = curl_exec($ch);
        curl_close($ch);        
        return $data;
    }

    /**
     * Realiza el login desde google
     */
    public function loginWithGoogleAction(Request $request){

        $em = $this->getDoctrine()->getManager();
        $dm = $this->get('doctrine_mongodb')->getManager();
        $data = $request->getContent();
        $data = json_decode($data, true);
        $atoken = $data["access_token"];
        $code = $data["code"];
        if(!$atoken || !$code)
            throw new \Exception("Token not found");
        
        //echo "token: ".$atoken."<br/>";
        $content = $this->getUserInfo($atoken);
        $data = json_decode($content, true);
        //echo "con google";
        //print_r($data);
        if(!is_array($data) || (is_array($data) && !array_key_exists("email", $data)))
            throw new \Exception("User not found");
        
        $repo = $em->getRepository("QestionBundle:User");
        $userName = $data["email"];
        $user = $repo->findOneByEmail($userName);
        
        if(!$user)
            throw new \Exception("User Not Registered", 403);
        
        $arr = $this->login($user);

        return $this->get("talker")->response($arr);
    }

    /**
     * Realiza el login de un usuario
     *
     * @param user - Nombre de usuario
     * @param pass - Password del usuario
     * @return arr - Array con los datos de logueo del usuario
     */
    public function loginAction(){
        $em = $this->getDoctrine()->getEntityManager();
        $dm = $this->get('doctrine_mongodb')->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        // $user = $this->get('security.context')->getToken()->getUser();        
        $repo = $em->getRepository("QestionBundle:User");
        $repoMongo = $dm->getRepository("QestionBundle:Session");
        $request = $this->get("request");
        if(!is_object($user))
        {
            if(isset($_GET['user']) && isset($_GET['pass'])){
                $userName = $_GET['user'];
                $password = $_GET['pass'];
            }

            if(isset($userName) && isset($password))
            {
                $pass = $this->generatePassword($password);
                $user = $repo->findOneByEmail($userName);
                if(!$user || $user->getPassword() != $pass)
                {

                    throw new \Exception("Usuario o contraseña incorecta", 403);
                }
            }
            else
            {
                throw new \Exception("Usuario o contraseña incorecta", 403);
            }
        }
        else{
            $userName = $user->getEmail();
        }
        $sess = $repoMongo->findOneBy(array("userAgent" => $request->headers->get('User-Agent'), 
            "email" => $userName, 
            "enabled" => true, 
            "cookie" => $request->headers->get("Cookie")));

        if($sess)
        {
            $session = $sess;
        }
        $arr = $this->login($user, $sess);
        return $this->get("talker")->response($arr);
    }

    private function login($user, $session = null)
    {
        $em = $this->getDoctrine()->getManager();
        $dm = $this->get('doctrine_mongodb')->getManager();
        $request = $this->get("request");
        if(is_null($session))
        {
            $userMongo = new \Qestion\QestionBundle\Document\User();
            $userMongo->setUserId($user->getId())
                // ->setName($user->getPerson()->getFirstname())
                // ->setLastname($user->getPerson()->getLastname())
                ->setEmail($user->getEmail());
                // ->setPhoto($user->getPerson()->getPhoto());
            $session = new \Qestion\QestionBundle\Document\Session();
            $session->setStart(new \MongoDate());
            $session->setUser($userMongo);
            $session->setIp($request->getClientIp());
            $session->setLast(new \MongoDate());
            $session->setEnabled(true);
            $session->setEmail($user->getUsername());
            $session->setUserAgent($request->headers->get("User-Agent"));
            $session->setCookie($request->headers->get("Cookie"));
            $dm->persist($session);
            $dm->flush();
        }
        $normalizer = $this->get("talker")->getNormalizer();
        $arr = $normalizer->normalizerMongo($session, "\Qestion\QestionBundle\Document\Session");
        $userNorm = $normalizer->normalize($user, 'Qestion\QestionBundle\Entity\User');
        $arr["email"] = $user->getEmail();
        unset($arr['cookie']);
        $userNorm = $normalizer->normalize($user, "Qestion\QestionBundle\Entity\User");
        // print_r($userNorm);
        $arr['user'] = array(
            "email" => $user->getEmail(),
            "name" => $user->getName(),
            "lastname" => $user->getLastname(),
            "photo" => $user->getImage(),
            "gender" => $user->getGender(),
            // "person" => $userNorm['person'],
            "roles" => $userNorm['roles'],
            "userId" => $user->getId(),
        );
        return $arr;
    }
    
    /**
     * Recibe un token de una sesion para cerrarla
     *
     * @param token - Token del usuario
     * @return array - Array con datos de la session cerrada
     */
    public function closeSessionAction(){
    	$dm = $this->get('doctrine_mongodb')->getManager();
        $request = $this->get("request");
    	$repo = $dm->getRepository("QestionBundle:Session");
        $sifincaRegex = '/SessionToken SessionID="([^"]+)", Username="([^"]+)"/';
        preg_match($sifincaRegex, $request->headers->get('x-qestion'), $matches);
        $token = $matches[1];
    	$sessionObj = $repo->findOneBy(array("id" => $token, "enabled" => true));
        if($sessionObj)
        {
            $sessionObj->setEnabled(false);
            $sessionObj->setEndDate(new \MongoDate());
            $dm->flush();
        }
        else
        {
            throw new \Exception("Session No registrada", 1);
        }
    	return $this->get("talker")->response(array("message" => "ok", "status" => 0, "timestamp" => $sessionObj->getEndDate()));
    }

    protected function afterList(&$Entity)
    {
        if(array_key_exists('data', $Entity))
        {
            foreach($Entity['data'] as $key => $value)
            {
                unset($Entity['data'][$key]['password']);
            }
        }
        else
        {
            unset($Entity['password']);
        }
    }

    /**
     * Cambiar la foto de perfil del usuario
     */
    public function changePhotoAction() {
         
        $em = $this->getDoctrine()->getManager();
        
        $user = $this->container->get('security.context')->getToken()->getUser();
         
        $dm = $this->get('doctrine_mongodb')->getManager();
        $request = $this->get("request");
    
        $uploadedFile = $request->files->all();
        $className = $this->namespace.$this->getName();
    
        $file = null;
        foreach($uploadedFile as $upFile)
        {
            $file = $upFile;
            break;
        }

        $image = new Image();
        $image->setName($file->getClientOriginalName());
        $image->setFile($file->getPathname());
        $image->setMimetype($file->getClientMimeType());
        $image->setEntity($className);
        $image->setEntityId($user->getId());
    
        $dm->persist($image);
        $dm->flush();
        $user->setImage($image->getId());
         
        $em->flush();
         
        return $this->get("talker")->response(array (
                "success" => true,
                "message" => $this->save_successful,
                "image" => $image->getId()
        ));
    }

}
?>