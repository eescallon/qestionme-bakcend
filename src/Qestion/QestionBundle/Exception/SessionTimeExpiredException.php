<?php
namespace Qestion\QestionBundle\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * AccessDeniedException is thrown when the account has not the required role.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class SessionTimeExpiredException extends HttpException
{

    public function __construct($message = 'Session Expired', \Exception $previous = null)
    {
    	$code = 401;
    	parent::__construct($code, $message, $previous, array(), $code);
    }
}
