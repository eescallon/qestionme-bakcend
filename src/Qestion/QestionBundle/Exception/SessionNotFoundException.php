<?php
namespace Qestion\QestionBundle\Exception;

/**
 * AccessDeniedException is thrown when the account has not the required role.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class SessionNotFoundException extends \RuntimeException
{
    public function __construct($message = 'Session Not Found', \Exception $previous = null)
    {
        parent::__construct($message, 401, $previous);
    }
}
