<?php
namespace Qestion\QestionBundle\Security;

use Symfony\Component\Security\Core\Authentication\SimplePreAuthenticatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\HttpUtils;

class ApiKeyAuthenticator implements SimplePreAuthenticatorInterface, AuthenticationFailureHandlerInterface
{
    protected $httpUtils;

    public function __construct(HttpUtils $httpUtils)
    {
        $this->httpUtils = $httpUtils;
    }

    public function createToken(Request $request, $providerKey)
    {
        $targetUrl = '/login';
        if ($this->httpUtils->checkRequestPath($request, $targetUrl)) {
            return;
        }
        $apiKey = $request->headers->get('x-qestion');
        if (!$apiKey) {
            throw new BadCredentialsException('No API key found');
        }

        return new PreAuthenticatedToken(
            'anon.',
            $apiKey,
            $providerKey
        );
    }

    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        if (!$userProvider instanceof UserProvider) {
            throw new \InvalidArgumentException(
                sprintf(
                    'The user provider must be an instance of UserProvider (%s was given).',
                    get_class($userProvider)
                )
            );
        }
        $apiKey = $token->getCredentials();
        $ses = $userProvider->getUsernameForApiKey($apiKey);
        if($ses)
        {
            if(array_key_exists('userName', $ses) && array_key_exists('sessionId', $ses))
            {
                $user = $userProvider->loadUserByUsername($ses['userName']);
                if($user){
                    $session = $userProvider->loadSessionBySessionIdAndUser($ses['sessionId'], $ses['userName']);
                    if($session){
                        return new PreAuthenticatedToken(
                            $user,
                            $apiKey,
                            $providerKey,
                            $user->getRoles()
                        );
                    }
                }
            
                throw new \Exception('The authentication failed.');
            }
        }
        else{
            throw new \Exception('The authentication failed.');
        }
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        throw new \Exception("Authentication Failed", 1);
        
    }

    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof PreAuthenticatedToken && $token->getProviderKey() === $providerKey;
    }
}
?>