<?php 
namespace Qestion\QestionBundle\Security;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\SessionUnavailableException;
use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthAwareUserProviderInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthUserProvider;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use Qestion\QestionBundle\Exception\SessionTimeExpiredException;
use Qestion\QestionBundle\Entity\User;

class UserProvider implements UserProviderInterface //implements UserProviderInterface, OAuthAwareUserProviderInterface
{
	private $em;
	private $cont;
	
	public function __construct($cont){
		$this->em = $cont->get('doctrine')->getManager();
		$this->cont = $cont;
	}
	
    public function loadUserByUsername($username)
    {
    	$request = $this->cont->get("request");
        $user = $this->cont->get('doctrine')->getManager()
                ->getRepository("QestionBundle:User")
                ->findOneBy(array("email" => $username, "deleted" => false));
        if($user)
            return $user;
    
        throw new UsernameNotFoundException(
            sprintf('Username "%s" does not exist.', $username)
        );
    }

    public function loadSessionBySessionIdAndUser($sessionId, $userName)
    {
        $request = $this->cont->get("request");
        $em = $this->cont->get('doctrine')->getManager();
        $dm = $this->cont->get('doctrine_mongodb')->getManager();
        $user = $em->getRepository("QestionBundle:User")->findOneByEmail($userName);

        if(!$user){
            throw new UsernameNotFoundException("Session not Registered", 401);
        }
        $repo = $dm->getRepository("QestionBundle:Session");
        $userId = $user->getId();
        $ses = $repo->findOneBy(array("id" => $sessionId, "user.userId" => intval($userId)));
        if(!$ses){
            throw new SessionUnavailableException("Session not Registered", 401);
        }else{
            if(!$ses->getFinalized())
            {
                if(!$ses->getEnabled())
                {
                    throw new SessionTimeExpiredException("Session Expired");
                }
                $session = $ses;
                if($session->getEnabled()){
                    $session->setLast(new \MongoDate());
                    $session->setIp($request->getClientIp());
                    $dm->flush();
                    return $session;
                }
            }
            else
            {
                
                $session->setLast(new \MongoDate());
                $session->setIp($request->getClientIp());
                $dm->flush();
                return $session;
            }
        }
    
        throw new UsernameNotFoundException(
            sprintf('Username "%s" does not exist.', $token->accessToken)
        );
    }

    public function getUsernameForApiKey($apiKey)
    {

        $regexByUserAndToken = '/SessionToken SessionID="([^"]+)", Username="([^"]+)"/';
        if(1 === preg_match($regexByUserAndToken, $apiKey, $matches)){
            $userName = $matches[2];
            $sessionId = $matches[1];
            return array("userName" => $userName, "sessionId" => $sessionId);
        }
        else{
            return;
        }
    }

    public function refreshUser(UserInterface $user)
    {
        return $this->loadUserByUsername($user->getUsername());
        if (!$user instanceof User) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return $class === 'Qestion\QestionBundle\Entity\User';
    }
}
?>
