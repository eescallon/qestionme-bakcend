<?php
namespace Qestion\QestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Game class
 * @ORM\Table(name="qest_questiongame")
 * @ORM\Entity(repositoryClass="Qestion\QestionBundle\Repository\MainRepository")
 */
class QuestionGame extends Entity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Question",cascade={"persist"})
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $question;

    /**
     * @ORM\ManyToOne(targetEntity="Game",cascade={"persist"})
     * @ORM\JoinColumn(name="game_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $game;
    
    /**
     * @ORM\Column(type="boolean")
     */
    private $checked = false;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set checked
     *
     * @param boolean $checked
     * @return QuestionGame
     */
    public function setChecked($checked)
    {
        $this->checked = $checked;

        return $this;
    }

    /**
     * Get checked
     *
     * @return boolean 
     */
    public function getChecked()
    {
        return $this->checked;
    }

    /**
     * Set question
     *
     * @param \Qestion\QestionBundle\Entity\Question $question
     * @return QuestionGame
     */
    public function setQuestion(\Qestion\QestionBundle\Entity\Question $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \Qestion\QestionBundle\Entity\Question 
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set game
     *
     * @param \Qestion\QestionBundle\Entity\Game $game
     * @return QuestionGame
     */
    public function setGame(\Qestion\QestionBundle\Entity\Game $game = null)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return \Qestion\QestionBundle\Entity\Game 
     */
    public function getGame()
    {
        return $this->game;
    }
}
