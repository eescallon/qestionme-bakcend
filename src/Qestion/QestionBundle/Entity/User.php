<?php
namespace Qestion\QestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Qestion\QestionBundle\Model\UserInterface;


/**
 * User class
 * @ORM\Table(name="qest_user", uniqueConstraints={@ORM\UniqueConstraint(name="email", columns={"email"})}))
 * @ORM\Entity(repositoryClass="Qestion\QestionBundle\Repository\MainRepository")
 */
class User extends Entity implements UserInterface
{
	/**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=100)
     * @Assert\Length(max=100)
     * @Assert\Email()
     */
    private $email;
    
    /**
     * @ORM\Column(type="string", nullable=true, length=100)
     * @Assert\Length(max=100)
     */
    private $name;
    
    /**
     * @ORM\Column(type="string", nullable=true, length=100)
     * @Assert\Length(max=100)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", nullable=true, length=1)
     * @Assert\Length(max=1)
     */
    private $gender;
    
    /**
     * @ORM\Column(type="string", nullable=true, length=100)
     * @Assert\Length(max=100)
     */
    private $image;
    
    /**
     * @ORM\ManyToMany(targetEntity="Role",cascade={"persist"})
     * @ORM\JoinTable(name="users_role",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     * )
     * @Assert\NotBlank()
     */
    private $roles;
    
    /**
     * @ORM\Column(type="string", length=40)
     * @Assert\Length(max=40)
     * @Assert\NotBlank()
     */
    private $password;
    
    /**
     * @ORM\Column(type="integer", nullable=true)        
     */
    private $wonGames = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $lostGames = 0;
    
    /**
     * @ORM\Column(type="boolean", nullable = true)      
     */
    private $active = true;
    
    /**
     * @ORM\Column(type="boolean", nullable = true)      
     */
    private $changePassword = true;


    public function serialize()
	{
		return serialize(array($this->getId(), $this->getUsername()));
	}

	public function unserialize($data)
	{
		list($this->id, $this->username)= unserialize($data);
	}

	public function getSalt()
	{
		return "qestion";
	}

	public function eraseCredentials()
	{
		return false;
	}

	public function equals(UserInterface $user)
	{
		return $user->getUsername() == $this->getUsername();
	}

	public function isAccountNonExpired()
	{
		return true;
	}

	public function isAccountNonLocked()
	{
		return true;
	}

	public function isCredentialsNonExpired()
	{
		return true;
	}

	public function isEnabled()
	{
		return true;
	}

    public function __construct()
    {
        $this->roles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getRoles()
    {
        return $this->roles->toArray();
    }

	public function getPassword()
    {
		return $this->password;
	}

	public function getUsername()
    {
		return $this->email;
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return User
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set wonGames
     *
     * @param integer $wonGames
     * @return User
     */
    public function setWonGames($wonGames)
    {
        $this->wonGames = $wonGames;

        return $this;
    }

    /**
     * Get wonGames
     *
     * @return integer 
     */
    public function getWonGames()
    {
        return $this->wonGames;
    }

    /**
     * Set lostGames
     *
     * @param integer $lostGames
     * @return User
     */
    public function setLostGames($lostGames)
    {
        $this->lostGames = $lostGames;

        return $this;
    }

    /**
     * Get lostGames
     *
     * @return integer 
     */
    public function getLostGames()
    {
        return $this->lostGames;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return User
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set changePassword
     *
     * @param boolean $changePassword
     * @return User
     */
    public function setChangePassword($changePassword)
    {
        $this->changePassword = $changePassword;

        return $this;
    }

    /**
     * Get changePassword
     *
     * @return boolean 
     */
    public function getChangePassword()
    {
        return $this->changePassword;
    }

    /**
     * Add roles
     *
     * @param \Qestion\QestionBundle\Entity\Role $roles
     * @return User
     */
    public function addRole(\Qestion\QestionBundle\Entity\Role $roles)
    {
        $this->roles[] = $roles;

        return $this;
    }

    /**
     * Remove roles
     *
     * @param \Qestion\QestionBundle\Entity\Role $roles
     */
    public function removeRole(\Qestion\QestionBundle\Entity\Role $roles)
    {
        $this->roles->removeElement($roles);
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }
}
