<?php
namespace Qestion\QestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Player class
 * @ORM\Table(name="qest_player", uniqueConstraints={@ORM\UniqueConstraint(name="uniqueplayer", columns={"game_id","user_id"})})
 * @ORM\Entity(repositoryClass="Qestion\QestionBundle\Repository\MainRepository")
 */
class Player extends Entity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Game",cascade={"persist"}, inversedBy="players")
     * @ORM\JoinColumn(name="game_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $game;

    /**
     * @ORM\ManyToOne(targetEntity="User",cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $player;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    private $points = 0;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    private $position;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set points
     *
     * @param integer $points
     * @return Player
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points
     *
     * @return integer 
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Set game
     *
     * @param \Qestion\QestionBundle\Entity\Game $game
     * @return Player
     */
    public function setGame(\Qestion\QestionBundle\Entity\Game $game = null)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return \Qestion\QestionBundle\Entity\Game 
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * Set player
     *
     * @param \Qestion\QestionBundle\Entity\User $player
     * @return Player
     */
    public function setPlayer(\Qestion\QestionBundle\Entity\User $player = null)
    {
        $this->player = $player;

        return $this;
    }

    /**
     * Get player
     *
     * @return \Qestion\QestionBundle\Entity\User 
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Player
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }
}
