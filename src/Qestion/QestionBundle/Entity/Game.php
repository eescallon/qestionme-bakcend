<?php
namespace Qestion\QestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Game class
 * @ORM\Table(name="qest_game")
 * @ORM\Entity(repositoryClass="Qestion\QestionBundle\Repository\QuestionRepository")
 */
class Game extends Entity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\Length(max=100)
     * @Assert\NotBlank()
     */
    private $name;
    
    /**
     * @ORM\OneToMany(targetEntity="Player", mappedBy="game", cascade={"persist"})
     */
    private $players;
    
    /**
     * @ORM\OneToMany(targetEntity="QuestionGame", mappedBy="game", cascade={"persist"})
     */
    private $questions;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    private $totalQuestions;
    
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    private $askedQuestion = 0;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    private $round = 1;

    /**
     * Estados del juego. P para pendiente, I para Iniciado, F para Finalizado
     * @ORM\Column(type="string", nullable=false, length=1)
     * @Assert\NotBlank()
     * @Assert\Length(max=1)
     */
    private $state;

    /**
     * @ORM\ManyToOne(targetEntity="Player",cascade={"persist"})
     * @ORM\JoinColumn(name="playerturn_id", referencedColumnName="id")
     */
    private $playerTurn;

    /**
     * @ORM\ManyToOne(targetEntity="Player",cascade={"persist"})
     * @ORM\JoinColumn(name="playeranswering_id", referencedColumnName="id")
     */
    private $playerAnswering;

    public function __construct()
    {
        $this->state = "P";
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Game
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set totalQuestions
     *
     * @param integer $totalQuestions
     * @return Game
     */
    public function setTotalQuestions($totalQuestions)
    {
        $this->totalQuestions = $totalQuestions;

        return $this;
    }

    /**
     * Get totalQuestions
     *
     * @return integer 
     */
    public function getTotalQuestions()
    {
        return $this->totalQuestions;
    }

    /**
     * Set askedQuestion
     *
     * @param integer $askedQuestion
     * @return Game
     */
    public function setAskedQuestion($askedQuestion)
    {
        $this->askedQuestion = $askedQuestion;

        return $this;
    }

    /**
     * Get askedQuestion
     *
     * @return integer 
     */
    public function getAskedQuestion()
    {
        return $this->askedQuestion;
    }

    /**
     * Add players
     *
     * @param \Qestion\QestionBundle\Entity\Player $players
     * @return Game
     */
    public function addPlayer(\Qestion\QestionBundle\Entity\Player $players)
    {
        $this->players[] = $players;

        return $this;
    }

    /**
     * Remove players
     *
     * @param \Qestion\QestionBundle\Entity\Player $players
     */
    public function removePlayer(\Qestion\QestionBundle\Entity\Player $players)
    {
        $this->players->removeElement($players);
    }

    /**
     * Get players
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * Add questions
     *
     * @param \Qestion\QestionBundle\Entity\QuestionGame $questions
     * @return Game
     */
    public function addQuestion(\Qestion\QestionBundle\Entity\QuestionGame $questions)
    {
        $this->questions[] = $questions;

        return $this;
    }

    /**
     * Remove questions
     *
     * @param \Qestion\QestionBundle\Entity\QuestionGame $questions
     */
    public function removeQuestion(\Qestion\QestionBundle\Entity\QuestionGame $questions)
    {
        $this->questions->removeElement($questions);
    }

    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return Game
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set playerTurn
     *
     * @param \Qestion\QestionBundle\Entity\Player $playerTurn
     * @return Game
     */
    public function setPlayerTurn(\Qestion\QestionBundle\Entity\Player $playerTurn = null)
    {
        $this->playerTurn = $playerTurn;

        return $this;
    }

    /**
     * Get playerTurn
     *
     * @return \Qestion\QestionBundle\Entity\Player 
     */
    public function getPlayerTurn()
    {
        return $this->playerTurn;
    }

    /**
     * Set playerAnswering
     *
     * @param \Qestion\QestionBundle\Entity\Player $playerAnswering
     * @return Game
     */
    public function setPlayerAnswering(\Qestion\QestionBundle\Entity\Player $playerAnswering = null)
    {
        $this->playerAnswering = $playerAnswering;

        return $this;
    }

    /**
     * Get playerAnswering
     *
     * @return \Qestion\QestionBundle\Entity\Player 
     */
    public function getPlayerAnswering()
    {
        return $this->playerAnswering;
    }

    /**
     * Set round
     *
     * @param integer $round
     * @return Game
     */
    public function setRound($round)
    {
        $this->round = $round;

        return $this;
    }

    /**
     * Get round
     *
     * @return integer 
     */
    public function getRound()
    {
        return $this->round;
    }
}
