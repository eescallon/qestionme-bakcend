<?php
namespace Qestion\QestionBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Qestion\QestionBundle\Entity\Role;
use Qestion\QestionBundle\Entity\User;

class TestCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('qestion:test')
            ->setDescription('Comando que realiza funciones de contabilidad');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->insertData();
    }

    private function insertData()
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $role = new Role();
        $role->setName('admin');
        $role->setDescription('Role para los Administradores');
        $em->persist($role);
        $user = new User();
        $user->setEmail("eduard.escallon@gmail.com");
        $user->addRol($role);
        $em->persist($user);
        $em->flush();
    }
}