<?php
namespace Qestion\QestionBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\yaml\Parser;
use Symfony\Component\yaml\Exception\ParseException;
use Symfony\Component\Yaml\Dumper;

class InsertInitialDataCommand extends ContainerAwareCommand
{
    private $em;

    protected function configure()
    {
        $this
            ->setName('qestion:insert:data')
            ->setDescription('Inserta los datos iniciales en la base de datos');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
                $this->ejectFunctions();
    }

    private function ejectFunctions()
    {
        $roles = array(
            'route' => __DIR__.'/../Resources/config/initialdata/roles.json',
            'namespace' => 'Qestion\QestionBundle\Entity\Role',
            'configuration' => array(
                'name' => 'name',
                'description' => 'description'
            )
        );
        $this->insertEntity($roles);

        $users = array(
            'route' => __DIR__.'/../Resources/config/initialdata/users.json',
            'namespace' => 'Qestion\QestionBundle\Entity\User',
            'configuration' => array(
                'email' => 'email',
                'name' => 'name',
                'lastname' => 'lastname',
                'roles' => 'roles'
            )
        );
        $this->insertEntity($users);

        $categories = array(
            'route' => __DIR__.'/../Resources/config/initialdata/categories.json',
            'namespace' => 'Qestion\QestionBundle\Entity\Category',
            'configuration' => array(
                'name' => 'name',
                'description' => 'description',
            )
        );
        $this->insertEntity($categories);

        $questions = array(
            'route' => __DIR__.'/../Resources/config/initialdata/questions2.json',
            'namespace' => 'Qestion\QestionBundle\Entity\Question',
            'configuration' => array(
                'question' => 'question',
                'category' => 'category',
                'checked' => 'checked',
            )
        );
        $this->insertEntity($questions);

        $answer = array(
            'route' => __DIR__.'/../Resources/config/initialdata/answers2.json',
            'namespace' => 'Qestion\QestionBundle\Entity\Answer',
            'configuration' => array(
                'answer' => 'answer',
                'question' => 'question',
                'selected' => 'selected',
            )
        );
        $this->insertEntity($answer);
        return true;
    }

    public function insertEntity($config)
    {
        $em = $this->getContainer()->get("doctrine")->getManager();
        $dm = $this->getContainer()->get('doctrine_mongodb')->getManager();
        $data = file_get_contents($config['route']);
        // print_r($data);
        echo $config['route']."\n";
        $data = json_decode($data, true);
        print_r($data);
        $return = array();
        foreach($data as $key => $val)
        {
            $class = new $config['namespace'];
            $md = $em->getClassMetadata($config['namespace']);
            $associations = $md->associationMappings;
            $fieldMapping = $md->fieldMappings;
            foreach($config['configuration'] as $k => $conf)
            {
                $explode = explode(".",$conf);
                $newVal;
                if(count($explode) > 1)
                {
                    $count = count($explode);
                    $newVal = $val;
                    for($i = 0; $i< $count; $i++)
                    {
                        if(is_array($newVal))
                        {
                            if(empty($newVal))
                            {
                                $newVal = null;
                            }
                            else
                            {
                                $newVal = $newVal[$explode[$i]];
                            }
                            
                        }
                        else
                        {
                            $newVal = $newVal->$explode[$i];
                        }
                    }
                }
                else
                {
                    if(is_array($val))
                    {
                        if(array_key_exists($conf, $val))
                            $newVal = $val[$conf];
                        else
                            $newVal = null;
                    }
                    else
                    {
                        $newVal = $val->$conf;
                    }
                }
                if(array_key_exists($k, $fieldMapping))
                {
                    $setter = 'set'.ucwords($k);
                    if(is_array($newVal))
                    {
                        $repository = $dm->getRepository($newVal['search']);
                        $document = $repository->findOneBy($newVal['parameters']);
                        if($document)
                            $class->$setter($document->getId());
                    }
                    else
                    {
                        if($fieldMapping[$k]['type'] == 'date' || $fieldMapping[$k]['type'] == 'datetime')
                        {
                            $class->$setter(new \DateTime($newVal));
                        }
                        else{
                            $class->$setter($newVal);
                        }
                    }
                }
                elseif(array_key_exists($k, $associations))
                {
                    if(array_key_exists($k, $val))
                    {
                        if($associations[$k]['type'] == 4 || $associations[$k]['type'] == 8)
                        {
                            if (substr ( $associations[$k]["fieldName"], - 1 ) == "s"){
                                // echo $key;
                                $setter = 'add' . ucwords ( substr ( $k, 0, - 1 ) );
                            }
                            else{
                                $setter = 'add' . ucwords ( $k );
                            }
                            echo "------------------------------- Valor del foreach ------------------------\n";
                            print_r($newVal);
                            foreach($newVal as $v)
                            {
                                $repository = $em->getRepository($associations[$k]['targetEntity']);
                                echo $k." - antes<br>";
                                print_r($v);
                                $object = $repository->findOneBy($v);
                                // echo $k." - pase<br>";
                                if($object){
                                    
                                    $class->$setter($object);
                                }
                            }
                        }
                        else
                        {
                            $setter = 'set'.ucwords($k);
                            $repository = $em->getRepository($associations[$k]['targetEntity']);
                            // echo $k." - antes2<br>";
                            $object = $repository->findOneBy($newVal);
                            // echo $k." - pase2<br>";
                            if($object){
                                $class->$setter($object);
                            }
                        }
                    }
                }
            }
            $em->persist($class);
            $return[] = $class;
            $em->flush();
        }
        return $return;
    }
}