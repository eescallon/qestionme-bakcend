<?php
namespace Qestion\QestionBundle\Model;

use Symfony\Component\Security\Core\User\AdvancedUserInterface;

/**
 * 
 */
interface UserInterface extends AdvancedUserInterface, \Serializable
{
	public function getRoles();
	
	public function getPassword();
	
	public function getSalt();
	
	public function getUsername();
	
	public function eraseCredentials();
	
	public function equals(UserInterface $user);
	
	public function isAccountNonExpired();
	
	public function isAccountNonLocked();
	
	public function isCredentialsNonExpired();
	
	public function isEnabled();
}