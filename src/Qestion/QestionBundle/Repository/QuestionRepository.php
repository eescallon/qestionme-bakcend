<?php

namespace Qestion\QestionBundle\Repository;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;

class QuestionRepository extends MainRepository
{
	public function getQuestionListNotIn(array $listId = array(), $asArray = true) {
		
		$query =  $this->createQueryBuilder('q')
						->select('q, r, c')
						->join('q.answers', 'r', 'WITH','r.deleted = false')
						->join('q.category', 'c', 'WITH','c.deleted = false')
						->where('q.deleted = false AND q.checked = true');
							
		if(!count($listId)==0) {		
			$query->andWhere('q.id NOT IN (:ids)')
				  ->setParameter('ids',$listId);
		}
		
		$query = $query->getQuery();
		//echo $query->getSQL()."<br/><br/>";

		if($asArray){
			return $query->getArrayResult();	
		}else{
			return $query->getResult();
		}
	}


	public function getQuestionList($asArray = true) {
		
		$query =  $this->createQueryBuilder('q')
						->select('q, r, c')
						->join('q.answers', 'r', 'WITH','r.deleted = false')
						->join('q.category', 'c', 'WITH','c.deleted = false')
						->where('q.deleted = false AND q.checked = true');
									
		$query = $query->getQuery();

		if($asArray){
			return $query->getArrayResult();	
		}else{
			return $query->getResult();
		}
	}

	public function getGameNotStarted($start = 0)
	{
		$query = $this->createQueryBuilder('g')
		->select("g,p")
		->leftJoin("g.players", "p", 'WITH', 'p.deleted = FALSE')
		->where("g.state = :sg AND g.deleted = FALSE")
		->setParameter("sg", "P")
		->orderBy("g.entrydate", "DESC");

		$query = $query->getQuery();
		$data = $query->getResult();
		if(count($data) == 0)
		{
			return array("total" => 0, "data" => "No hay partidas pendientes");
		}
		$data = $data[0];
		if(count($data->getPlayers()) < 4)
		{
			return array("total" => 1, "data" => $data);
		}
		else{
			$start++;
			$this->getGameNotStarted($start);
		}
	}
}