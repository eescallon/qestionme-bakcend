<?php
namespace Qestion\QestionBundle\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;

class MainMongoRepository extends DocumentRepository
{
	public function consultDocument($dm, $document, $id = null, array $criteria = null, array $filter = null, $nameBundle = null, $skip = null, $pageSize = null)
    {
        //$dm = $this->getDocumentManager();
        $sql = $this->createQueryBuilder($document)
        ->field('deleted')->equals(false);
        if(!is_null($id))
        {
            $sql->field('id')->equals($id);
            $sql = $sql->getQuery()->execute();
            return $sql->toArray();
        }
        if(!is_null($criteria))
        {
            foreach($criteria as $key => $value)
            {
                $sql->field($key)->equals($value);
            }
        }
        if(!is_null($filter))
        {
            $this->setFilterToQuery($dm, $sql, $filter, $document, $nameBundle);
        }
        $total = null;
        if(!is_null($skip) && !is_null($pageSize))
        {
            $sql = $sql->limit($pageSize)->skip($skip);
            $total = $sql->getQuery()->execute()->count();
            
        }
        $sql = $sql->getQuery()->execute();

        if(is_null($skip) && is_null($pageSize)){
            $total = count($sql);
        }

        $data = array(
            "total" => $total,
            "data" => $sql->toArray()
        );

        return $data;
    }

    public function setFilterToQuery($dm, &$sql, $filter, $document, $nameBundle)
    {
        //$dm = $this->getDocumentManager();
        $val1 = null;
        $val2 = null;
        $md = $dm->getClassMetadata($nameBundle.":".$document);
        $fieldMappings = $md->fieldMappings;
        foreach($filter as $value)
        {
            $property = $value['property'];
            $val = $this->getValue($value['value']);
            $operator = trim($value['operator']);
            $sql->field($property)->equals($val);
            $fmType = $fieldMappings[$property]['type'];
            if($fmType == "date")
            {
                if(is_array($val))
                {
                    $first = new \MongoDate(strtotime($val[0]));
                    $second = new \MongoDate(strtotime($val[1]." 23:59:59"));
                }
                else
                {
                    if($operator == "<=" || $operator == ">")
                    {
                        $val = new \MongoDate(strtotime($val." 23:59:59"));
                    }
                    elseif($operator == "=")
                    {
                        $val1 = new \MongoDate(strtotime($val));
                        $val2 = new \MongoDate(strtotime($val." 23:59.59"));
                    }
                    else
                    {
                        $val = new \MongoDate(strtotime($val));
                    }
                }
            }
            elseif($fmType == "int" || $fmType == "float")
            {
                if(is_array($val))
                {
                    $first = intval($val[0]);
                    $second = intval($val[1]);
                }
                else
                {
                    $val = intval($val);
                }
            }
            else
            {
            }
            switch($operator)
            {
                case "=":
                    if($fmType == "date")
                    {
                        $sql->field($property)->range($val1, $val2);
                    }
                    else
                    {
                        switch($val)
                        {
                            case "true":
                                $sql->field($property)->equals(true);
                                break;
                            case "false":
                                $sql->field($property)->equals(false);
                                break;
                            default:
                                echo $property;
                                $sql->field($property)->equals($val);
                                break;
                        }
                    }
                    break;
                case ">":
                    $sql->field($property)->gt($val);
                    break;
                case "<":
                    $sql->field($property)->lt($val);
                    break;
                case ">=":
                    $sql->field($property)->gte($val);
                    break;
                case "<=":
                    $sql->field($property)->lte($val);
                    break;
                case "has":
                    $sql->field($property)->equals(new \MongoRegex('/.*'.$val.'.*/i'));
                    break;
                case "equal":
                    switch($val)
                    {
                        case "true":
                            $sql->field($property)->equals(true);
                            break;
                        case "false":
                            $sql->field($property)->equals(false);
                            break;
                        default:
                            $sql->field($property)->equals($val);
                            break;
                    }
                    break;
                case "start_with":
                    $sql->field($property)->equals(new \MongoRegex('/^'.$val.'/i'));
                    break;
                case "end_with":
                    $sql->field($property)->equals(new \MongoRegex('/^(.)*('.$val.')$/i'));
                    break;
                case "between":
                    $sql->field($property)->range($first, $second);
                    break;
                case "empty":
                    $sql->field($property)->equals("");
                    break;
            }
        }
        return $sql;
    }

    public function searchExpireSession($date)
    {
        $searchDate = new \MongoDate(strtotime($date));
        $dm = $this->getDocumentManager();
        $query = $dm->createQueryBuilder('QestionBundle:Session')
                    ->field('enabled')->equals(true)
                    ->field("last")->lte($searchDate)
                    ->getQuery()
                    ->execute();
        return $query;
    }

    private function getValue($value){
        $valueSplode =  explode("=return",$value);

        if(count($valueSplode) > 1){
            $evalValue = eval("return ".$valueSplode[1]);            
            if($evalValue){
                $value = $evalValue;
            }else{
                $value = "";
            }
        }
        return $value;
    }
}
?>