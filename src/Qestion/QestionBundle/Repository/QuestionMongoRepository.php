<?php
namespace Qestion\QestionBundle\Repository;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;

class QuestionMongoRepository extends MainMongoRepository
{
	public function getAnswersByQuestionAndGame($gameId, $questionId)
	{
		$query = $this->createQueryBuilder("QestionBundle:TurnQuestion")
					->field('gameId')->equals($gameId)
					->field('questionId')->equals($questionId)
					->getQuery()
                    ->execute();
        return $query;
	}
}