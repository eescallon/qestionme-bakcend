<?php
namespace Qestion\QestionBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Qestion\QestionBundle\Exception\SessionNotFoundException;

class ExceptionListener
{
    private $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        // You get the exception object from the received event
        $exception = $event->getException();

        if(method_exists($exception, "getStatusCode"))
        {
            $code = $exception->getStatusCode();
        }
        else{
            $code = $exception->getCode();
        }
        if($code == intval(0) || $code == "0" || $code == "" || $code < 200)
        {
            $code = 500;
        }
        $format = array(
            "message" => $exception->getMessage(),
            "code" => $code
        );
        $type = $this->request->getType();
        // Customize your response object to display the exception details
        $response = new Response();
        $response->setContent(json_encode($format));
        $response->headers->set('Content-Type', $type['type']);
        // HttpExceptionInterface is a special type of exception that
        // holds status code and header details
        if ($exception instanceof HttpExceptionInterface) {
            // $response->setStatusCode($exception->getStatusCode());
            $response->headers->replace($exception->getHeaders());
        }
        $response->setStatusCode($code);

        // Send the modified response object to the event
        $event->setResponse($response);
    }
}