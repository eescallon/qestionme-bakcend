<?php
namespace Qestion\QestionBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;

class SerialListener
{
	private $container;

	public function __construct($container)
	{
		$this->container = $container;
	}

	public function prePersist(LifecycleEventArgs $args)
	{
		$entity = $args->getEntity();
		$em = $args->getEntityManager();		

		if($entity instanceof \Qestion\QestionBundle\Entity\User)
		{
			$pass = $this->generatePassword(8);
			$factory = $this->container->get('security.encoder_factory');
			$codificador = $factory->getEncoder($entity);
			$password = $codificador->encodePassword('qestion123', $entity->getSalt());
			$entity->setPassword($password);
		}
		if($entity instanceof \Qestion\QestionBundle\Entity\Player)
		{
			$game = $entity->getGame();
			$count = count($game->getPlayers());
			$entity->setPosition($count+1);
		}
	}

	private function generatePassword($longitud){ 
		$cadena="[^A-Z0-9]"; 
		return substr(eregi_replace($cadena, "", md5(rand())) . 
			eregi_replace($cadena, "", md5(rand())) . 
			eregi_replace($cadena, "", md5(rand())), 
			0, $longitud
		); 
	} 
}