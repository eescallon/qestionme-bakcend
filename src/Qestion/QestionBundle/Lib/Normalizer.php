<?php
namespace Qestion\QestionBundle\Lib;

use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;


class Normalizer extends GetSetMethodNormalizer
{
	private $em;
	private $dm;
	private $container;

	private $propertyChanged = array();

	private $ignoreProperties = array("id", "entrydate", "lastupdate");

	private function setPropertyChanges($propertyName, $oldValue, $newValue, $entityId, $action){
		$this->propertyChanged[] = array(
			"propertyName" => $propertyName,
			"oldValue" => $oldValue,
			"newValue" => $newValue,
			"entityId" => $entityId,
			"action" => $action
		);
	}

	public function getPropertyChanges()
	{
		return $this->propertyChanged;
	}

	public function setEntityManager($em) {
		$this->em = $em;
	}

	public function setDocumentManager($dm) {
		$this->dm = $dm;
	}

	public function setContainer($co)
	{
		$this->container = $co;
	}

	private function isGetMethod(\ReflectionMethod $method)
	{
		return (
				0 === strpos($method->name, 'get') &&
				3 < strlen($method->name) &&
				0 === $method->getNumberOfRequiredParameters()
		);
	}

	public function clearIgnoredAttributes()
	{
		$this->ignoredAttributes = array();
	}

	/**
	 * Funcion para normalizar un documento de mongodb (Convertir de objeto a array)
	 */
	public function normalizerMongo($document, $class, $print = false){
        $md = $this->dm->getClassMetadata($class);
        $fm = $md->fieldMappings;
        $arr = array();
        foreach($fm as $key => $value)
        {
        	$getter = 'get'.ucfirst($key);
        	if(method_exists($document, $getter))
        	{
	        	if($value['type'] == 'one' || $value['type'] == 'many')
	        	{
	        		$val = $document->{'get'.ucfirst($key)}();
	        		if($value['type'] == 'many'){
	        			foreach ($val->toArray() as $k => $v) {
	        				// print_r($v);
	        				$arr[$key][] = $this->normalizerMongo($v, $value['targetDocument'], true);
	        			}
	        		}else{
	        			$arr[$key] = $this->normalizerMongo($val, $value['targetDocument']);
	        		}
	        	}
	        	else
	        	{
	        		$val = $document->{'get'.ucfirst($key)}();
	        		$arr[$key] = $val;
	        	}
        	}
        }
        return $arr;
    }

	/**
	 * Funcion para normalizar una entidad (Convertir de objeto a array)
	 */
	public function normalize($object, $format = null, array $context = array(), $recursive = true, $rn = 0)
	{
		$reflectionObject = new \ReflectionObject($object);
		$reflectionMethods = $reflectionObject->getMethods(\ReflectionMethod::IS_PUBLIC);
		$attributes = array();
		$objectName = $reflectionObject->getName();
		if(substr($objectName, 0,1)!="\\") $objectName = "\\".$objectName;
		$md = $this->em->getClassMetadata($objectName);
		$associations = $md->associationMappings;
		$fm = $md->fieldMappings;
		foreach($fm as $key => $value)
		{
			$getter = "get" . ucwords ( $key );
			$pos = strrpos($getter, "_");
			if($pos !== false)
			{
				$getter = substr($getter, 0, $pos) . ucwords(substr($getter, $pos+1));
			}
			if(method_exists($object, $getter))
			{
				$attributes[$key] = $object->{$getter}();
			}
		}
		foreach($associations as $key => $value)
		{
			$getter = "get" . ucwords ( $key );
			$pos = strrpos($getter, "_");
			if($pos !== false)
			{
				$getter = substr($getter, 0, $pos) . ucwords(substr($getter, $pos+1));
			}
			if(!method_exists($object, $getter))
			{
				continue;
			}
			$var = $object->{$getter}();
			if(is_null($var))
			{
				continue;
			}
			if ($value ["type"] == 4 || $value ["type"] == 8) {
				$entities = array();
				if($recursive){
					foreach($var as $entity)
					{
						if($value["type"] == 8)
							$entities[] = $this->normalize($entity, $format, $context, false, $rn++);
						else
							$entities[] = $this->normalize($entity, $format, $context, true, $rn++);
					}
					$var = $entities;
				}
			}
			else{
				if($recursive)
				{
					$var = $this->normalize($var, $format, $context, false, $rn++);
				}
			}
			$attributes[$key] = $var;
		}
		// print_r(json_encode($reflectionMethods));
		// foreach ($reflectionMethods as $method) {
		// 	if ($this->isGetMethod($method)) {
		// 		$attributeName = lcfirst(substr($method->name, 3));

		// 		// echo $attributeName;
		// 		// echo "\n";
		// 		if (in_array($attributeName, $this->ignoredAttributes)) continue;

		// 		$attributeValue = $method->invoke($object);
		// 		// if(is_object($attributeValue)){
		// 		// 	echo get_class($attributeValue);
		// 		// 	echo "\n";
		// 		// }
				
		// 		if (array_key_exists($attributeName, $this->callbacks))
		// 			$attributeValue = call_user_func($this->callbacks[$attributeName], $attributeValue);
		// 		if (null !== $attributeValue && !is_scalar($attributeValue)) {

		// 			if(array_key_exists($attributeName, $associations)){
		// 				$me = $associations[$attributeName];
		// 				if($me["type"] == 4 || $me["type"] == 8){
		// 					$entities = array();
		// 					if($recursive){
		// 						foreach($attributeValue as $entity)
		// 						{
		// 							if($me["type"] == 8)
		// 								$entities[] = $this->normalize($entity, $format, $context, false, $rn++);
		// 							else
		// 								$entities[] = $this->normalize($entity, $format, $context, true, $rn++);
		// 						}
		// 						$attributeValue = $entities;
		// 					}
		// 				}
		// 				else
		// 				{
		// 					if($recursive)
		// 					{
		// 						$attributeValue = $this->normalize($attributeValue, $format, $context, false, $rn++);
		// 					}
		// 				}

		// 			}
		// 		}
		// 		// echo $attributeName . " = " ;
		// 		// print_r($attributeValue);
		// 		// echo "<br><br><br>";
		// 		$attributes[$attributeName] = $attributeValue;
		// 	}
		// }

		return $attributes;
	}

	public function denormalizeDocument($data, $class)
	{
		$reflectionClass = new \ReflectionClass($class);

		$constructor = $reflectionClass->getConstructor();
		if($constructor)
		{
			$constructorParameters = $constructor->getParameters();
			$params = array();
			foreach ($constructorParameters as $constructorParameter) {
				
				$paramName = strtolower($constructorParameter->getName());
				if (isset($data[$paramName])) {
					$params[] = $data[$paramName];
					unset($data[$paramName]);
				} elseif (!$constructorParameter->isOptional()) {
					throw new RuntimeException(
							'Cannot create an instance of ' . $class .
							' from serialized data because its constructor requires ' .
							'parameter "' . $constructorParameter->getName() .
							'" to be present.');
				}
			}

			$newClass = $reflectionClass->newInstanceArgs($params);

		}
		else{
			$newClass = new $class;
		}
		$dm = $this->dm;
		// Obtiene la metadata de la clase a denormalizar
		$metadata = $dm->getClassMetadata($class);
		// Obtiene las asociaciones de la metadata
		$associations = $metadata->associationMappings;
		// Obtiene los atributos de la metadata
		$fieldMapping = $metadata->fieldMappings;
		// print_r($metadata);
		if(is_numeric($data)){
			// En caso de que la data recibida sea un numero, devuelve un objeto de la clase a denormalizar 
			return $dm->getRepository($class)->find($data);
		}
		$isNew = false;
		if(is_array($data) && array_key_exists($metadata->identifier, $data) &&
				!is_null($data[$metadata->identifier]) && !empty($data[$metadata->identifier])){
			//En caso de que la data sea un array, el identificador de la clase venga en la data, no sea nulo el identificador y no este vacio
			//busca en la base de datos un objeto de la clase a denormalizar segun ese id
			$isNew=true;
			$newClass = $dm->getRepository($class)->find($data[$metadata->identifier]);
		}
		foreach ($data as $key => $value) {
			if(in_array($key, $this->ignoreProperties)) continue;
			if(!empty($value) || is_bool($value)){
				$getter = "get" . ucwords ( $key );
				$setter = "set" . ucwords ( $key );
				if (array_key_exists ( $key, $associations )) {
					
					$asso = $associations [$key];
					
					$entity = $asso["targetEntity"];

					$mappedBy = $asso["mappedBy"];
					$mbSetter = null;
					if($asso ["type"] == 4)
						$mbSetter = "set" . ucwords ( $mappedBy );
					
					if($asso ["type"] == 8 && !empty($mappedBy))
						if (substr ( $mappedBy, - 1 ) == "s")
							$mbSetter = 'add' . ucwords ( substr ( $mappedBy, 0, - 1 ) );
						else
							$mbSetter = 'add' . ucwords ( $mappedBy );

					//echo "\n\n\n --------------------------------- Voy a entrar a obtener $entity --------------------------------\n\n";
					$obj = null;
					if (is_array($value)){
						$keys = array_keys ($value);
						if(is_numeric ( $keys [0] )) {
							$obj = array ();
							// echo count($value);
							foreach ( $value as $v ) {
								// $newObj = null;
								if(!array_key_exists($metadata->identifier, $v) || is_null($v[$metadata->identifier]) || $v[$metadata->identifier] == "" || $asso ["type"] != 4)
								{
									// echo "entre if\n";
									$newObj = $this->denormalizeMongo ( $v, $entity, $format);
								}else{
									// echo "entre else\n";
									$newObj = $dm->getRepository($entity)->getEntity($v[$metadata->identifier]);
								}
								if(!is_null($mbSetter) && !is_null($newObj)){
									// echo "entre if2\n";
									$newObj->$mbSetter($newClass);
								}
								$obj [] = $newObj;
							}
						}else{
							// echo "\n - ".$value[$metadata->identifier[0]];
							if(!array_key_exists($metadata->identifier, $value) || is_null($value[$metadata->identifier]) || $value[$metadata->identifier] == "" || $asso ["type"] != 4)
							{
								$obj = $this->denormalizeMongo ( $value, $entity, $format);
							}
							else{

								$obj = $dm->getRepository($entity)->getEntity(array("id" => $value[$metadata->identifier]));
							}
							
							if(!is_null($mbSetter)) $obj->$mbSetter($newClass);
						}
					}else{
						$obj = $this->denormalizeMongo ( $value, $entity, $format);
						if(!is_null($mbSetter)) $obj->$mbSetter($newClass);
					}
					
					//echo "\n\n\n --------------------------------- FIN --------------------------------\n\n";
					
					if ($asso ["type"] == 4 || $asso ["type"] == 8) {
						if (substr ( $asso ["fieldName"], - 1 ) == "s")
							$setter = 'add' . ucwords ( substr ( $key, 0, - 1 ) );
						else
							$setter = 'add' . ucwords ( $key );

						$getter = 'get' . ucwords ( $key );
						
						$collection = $newClass->$getter();
						if(is_object($collection)){
							if($collection and $collection->count() > 0){
								// echo count($obj);
								$foundInCollection = $this->lookInCollection ( $obj, $collection);
								$toDelete = $this->removeNotBe($obj, $collection);

								if (substr ( $asso ["fieldName"], - 1 ) == "s")
									$removeMethod = 'remove' . ucwords ( substr ( $key, 0, - 1 ) );
								else
									$removeMethod = 'remove' . ucwords ( $key );

								foreach ($toDelete as $key => $ent) {
									$newClass->$removeMethod($ent);
									if($asso ["type"] == 4){
										// echo "hola";
										$em->remove($ent);
									}
								}

								if(!is_array($foundInCollection) && !$foundInCollection){

									if(is_array($obj)){
										foreach ($obj as $item) {
											$newClass->$setter($item);
										}
									}else{
										$newClass->$setter($obj);
									}
								}else if(is_array($foundInCollection)){

									foreach ($foundInCollection as $k => $o) {

										if(!$o){
											foreach ($obj as $item) {
												if($k == $item->getId())
													$newClass->$setter($item);
											}
										}
									}
								}
							}else{
								if(is_array($obj)){
									foreach ($obj as $item) {
										$newClass->$setter($item);
									}
								}else{
									$newClass->$setter($obj);
								}
							}
						}
					}else{
						if (method_exists ( $newClass, $setter )) {
							$newClass->$setter($obj);
						}
					}
				
				} elseif (array_key_exists ( $key, $fieldMapping )) {
					if ($fieldMapping [$key] ["type"] == "datetime" ||
							$fieldMapping [$key] ["type"] == "date" ||
							$fieldMapping [$key] ["type"] == "time") {
								$date = $value;
								if(is_array($date) && array_key_exists("date", $date)){
									$value = new \DateTime ( $date["date"] );
								}elseif(is_string($date)){
									$value = new \DateTime ( $date );
								}else{
									$value = new \DateTime ();
								}
					}

					
					
					if (method_exists ( $newClass, $setter )) {
						// echo $value."\n";
						// echo $value." - ";
						if($fieldMapping [$key] ["type"] == "boolean")
						{
							// echo $value;
							if($value === "false")
							{
								// echo "entre2";
								$newClass->$setter(false);
							}
							else
							{
								// echo "entre";
								$newClass->$setter($value);
							}
							
						}
						else
						{
							$newClass->$setter ( $value );
						}
						
						
						
						// echo $newClass->getQuantity();
					}
				}
			}
		}
		return $newClass;
	}

	/**
	 * Convierte un array en un objeto
	 * @param unknown_type $data
	 * @param unknown_type $class
	 * @param unknown_type $format
	 * @param unknown_type $context
	 * @throws RuntimeException
	 * @return Ambigous <unknown, object>
	 */
	public function denormalize($data, $class, $format = null, array $context = array(), $persist = false) {
		//TODO: Auto-generated method stub
		//Obtiene la informacion de una clase
		$reflectionClass = new \ReflectionClass($class);
		// Obtiene el constructor de la clase reflejada
		$constructor = $reflectionClass->getConstructor();
		if ($constructor) {
			// Obtiene los parametros del constructor
			$constructorParameters = $constructor->getParameters();
			$params = array();
			foreach ($constructorParameters as $constructorParameter) {
				
				$paramName = strtolower($constructorParameter->getName());
				if (isset($data[$paramName])) {
					$params[] = $data[$paramName];
					unset($data[$paramName]);
				} elseif (!$constructorParameter->isOptional()) {
					throw new RuntimeException(
							'Cannot create an instance of ' . $class .
							' from serialized data because its constructor requires ' .
							'parameter "' . $constructorParameter->getName() .
							'" to be present.');
				}
			}
			// Crea un nuevo objeto de la clase a denormalizar
			$newClass = $reflectionClass->newInstanceArgs($params);
			// echo get_class($newClass)."\n";
		} else {
			// Crea un nuevo objeto de la clase a denormalizar
			$newClass = new $class;
			// echo get_class($newClass)."\n";
		}
		 
		$em = $this->em;
		// Obtiene la metadata de la clase a denormalizar
		$metadata = $em->getClassMetadata($class);
		// Obtiene las asociaciones de la metadata
		$associations = $metadata->associationMappings;
		// Obtiene los atributos de la metadata
		$fieldMapping = $metadata->fieldMappings;
		if(is_numeric($data)){
			// En caso de que la data recibida sea un numero, devuelve un objeto de la clase a denormalizar 
			return $em->getRepository($class)->find($data);
		}
		$isNew = false;
		if(is_array($data) && array_key_exists($metadata->identifier[0], $data) &&
				!is_null($data[$metadata->identifier[0]]) && !empty($data[$metadata->identifier[0]])){
			//En caso de que la data sea un array, el identificador de la clase venga en la data, no sea nulo el identificador y no este vacio
			//busca en la base de datos un objeto de la clase a denormalizar segun ese id
			$isNew=true;
			$newClass = $em->getRepository($class)->find($data[$metadata->identifier[0]]);
		}
		foreach($associations as $association)
		{
			// print_r($association);
			if($association['targetEntity'] == "Sifinca\SifincaBundle\Entity\Company" && $class != 'Sifinca\SifincaBundle\Entity\User' && ($association['type'] == 1 || $association['type'] == 2))
			{
				// print_r($association);
				$company = $this->container->get('security.context')->getToken()->getUser()->getCurrentCompany();
				$newClass->setCompany($company);
			}
		}
		// print_r($data);
		foreach ($data as $key => $value) {
			if(in_array($key, $this->ignoreProperties)) continue;
			if(!empty($value) || is_bool($value)){
				// print_r($key);
				$getter = "get" . ucwords ( $key );
				$setter = "set" . ucwords ( $key );
				if (array_key_exists ( $key, $associations )) {
					$asso = $associations [$key];
					
					$entity = $asso["targetEntity"];

					$mappedBy = $asso["mappedBy"];
					$mbSetter = null;
					if($asso ["type"] == 4)
						$mbSetter = "set" . ucwords ( $mappedBy );

					if($asso ["type"] == 8 && !empty($mappedBy))
						if (substr ( $mappedBy, - 1 ) == "s")
							$mbSetter = 'add' . ucwords ( substr ( $mappedBy, 0, - 1 ) );
						else
							$mbSetter = 'add' . ucwords ( $mappedBy );

					//echo "\n\n\n --------------------------------- Voy a entrar a obtener $entity --------------------------------\n\n";
					$obj = null;
					$dontSet = array();
					if (is_array($value)){
						$keys = array_keys ($value);
						if(is_numeric ( $keys [0] )) {
							$obj = array ();
							// print_r($value);
							foreach ( $value as $v ) {
								// echo $key."<br>";
								$newObj = null;
								// print_r($v);
								if(!array_key_exists($metadata->identifier[0], $v) || is_null($v[$metadata->identifier[0]]) || $v[$metadata->identifier[0]] == "")
								{
									$newObj = $this->denormalize ( $v, $entity, $format);
									if(array_key_exists('id', $data)){
										$objectToCompare = $em->getRepository($class)->find($data['id']);
										$getter2 = 'get'.ucwords($key);
										$properties = $objectToCompare->$getter2();
										$found = false;
										foreach($properties as $prop)
										{
											if($prop->getName() == $newObj->getName())
											{
												$found = true;
												$dontSet[$key] = $newObj->getName();
												break;
											}
										}
										if(!$found){
											$this->setPropertyChanges($key, null, $newObj->getName(), $data['id'], 'POST');
										}
									}
									else
									{
										$this->setPropertyChanges($key, null, $newObj->getName(), null, 'POST');
									}
								}else{
									$changes = $this->hasChanges($v, $entity);
									if($changes){
										$newObj = $this->denormalize ( $v, $entity, $format);
										// $this->setPropertyChanges($key, null, $newObj->getName(), $data['id'], 'POST');
									}
									else{
										$newObj = $em->getRepository($entity)->getEntity($v[$metadata->identifier[0]]);
									}
									// if(array_key_exists('id', $data)){
									// 	if($newClass->$getter())
									// 	{
									// 		if($newClass->$getter()->getName() != $newObj->getName())
									// 			$this->setPropertyChanges($key, $newClass->$getter()->getName(), $newObj->getName(), $data['id'], 'PUT');
									// 	}
									// 	else
									// 	{
									// 		$this->setPropertyChanges($key, null, $newObj->getName(), $data['id'], 'POST');
									// 	}
									// }
								}
								if(!is_null($mbSetter) && !is_null($newObj)){
									// echo "entre if2\n";
									$newObj->$mbSetter($newClass);
								}
								$obj [] = $newObj;
							}
						}else{
							if((!array_key_exists($metadata->identifier[0], $value) || is_null($value[$metadata->identifier[0]]) || $value[$metadata->identifier[0]] == "") && $asso ["type"] != 4)
							{
								if($key == "identity"){
									$obj = $this->denormalize ( $value, $entity, $format, array());
								}
								else{
									$obj = $this->denormalize ( $value, $entity, $format, array(), true);
								}
								if(!is_null($obj->getName()))
								{
									$this->setPropertyChanges($key, null, $obj->getName(), null, 'POST');
								}
							}
							else{
								$changes = $this->hasChanges($value, $entity);
								if($changes){
									$obj = $this->denormalize ( $value, $entity, $format);
								}
								else{
									// echo "entre cuando no hay cambios en $entity\n";
									$obj = $em->getRepository($entity)->getEntity($value[$metadata->identifier[0]]);
								}
								if(array_key_exists('id', $data)){
									if($newClass->$getter())
									{
										if($newClass->$getter()->getName() != $obj->getName())
											$this->setPropertyChanges($key, $newClass->$getter()->getName(), $obj->getName(), $data['id'], 'PUT');
									}
									else
									{
										$this->setPropertyChanges($key, null, $obj->getName(), $data['id'], 'POST');
									}
								}
								else
								{
									$this->setPropertyChanges($key, null, $obj->getName(), null, 'POST');
								}
							}
							
							if(!is_null($mbSetter)){
								$obj->$mbSetter($newClass);
							}
						}
					}else{
						$obj = $this->denormalize ( $value, $entity, $format);
						if(!is_null($mbSetter)) $obj->$mbSetter($newClass);
					}
					// echo "\n\n\n --------------------------------- FIN --------------------------------\n\n";
					// print_r($asso);
					// echo $getter."\n";
					if ($asso ["type"] == 4 || $asso ["type"] == 8) {
						if (substr ( $asso ["fieldName"], - 1 ) == "s"){
							// echo $key;
							$setter = 'add' . ucwords ( substr ( $key, 0, - 1 ) );
						}
						else{
							// echo $key;
							$setter = 'add' . ucwords ( $key );
						}
						$getter = 'get' . ucwords ( $key );
						$collection = $newClass->$getter();
						// echo count($collection);
						// echo get_class($collection)." - ".$key."<br>";
						if(is_object($collection)){
							if($collection and $collection->count() > 0){
								// echo count($obj);
								$foundInCollection = $this->lookInCollection ( $obj, $collection);
								$toDelete = $this->removeNotBe($obj, $collection);

								if (substr ( $asso ["fieldName"], - 1 ) == "s")
									$removeMethod = 'remove' . ucwords ( substr ( $key, 0, - 1 ) );
								else
									$removeMethod = 'remove' . ucwords ( $key );
								// echo "<br>$key ".count($toDelete);
								foreach ($toDelete as $key => $ent) {

									$newClass->$removeMethod($ent);
									if($asso ["type"] == 4){
										$found = false;
										foreach($dontSet as $property => $valueProp)
										{
											if($valueProp == $ent->getName())
											{
												$found = true;
												break;
											}
										}
										if(!$found){
											$this->setPropertyChanges($key, $ent->getName(), null, $data['id'], 'DELETE');
										}
										$em->remove($ent);
									}
								}
								if(!is_array($foundInCollection) && !$foundInCollection){
									if(is_array($obj)){
										foreach ($obj as $item) {
											$newClass->$setter($item);
										}
									}else{
										$newClass->$setter($obj);
									}
								}else if(is_array($foundInCollection)){
									foreach ($foundInCollection as $k => $o) {
										if(!$o){
											foreach ($obj as $item) {
												if($k == $item->getId()){
													$newClass->$setter($item);
												}
											}
										}
									}
								}
							}else{
								if(is_array($obj)){
									foreach ($obj as $item) {
										$newClass->$setter($item);
									}
								}else{
									$newClass->$setter($obj);
								}
							}
						}
						else{
							if(is_array($obj)){
								foreach ($obj as $item) {
									
									$newClass->$setter($item);
								}
							}else{

								$newClass->$setter($obj);
							}
						}
					}else{
						if (method_exists ( $newClass, $setter )) {
							// echo $class."\n";
							// print_r($data);
							$newClass->$setter($obj);
						}
					}
							
				} elseif (array_key_exists ( $key, $fieldMapping )) {
					if ($fieldMapping [$key] ["type"] == "datetime" ||
							$fieldMapping [$key] ["type"] == "date" ||
							$fieldMapping [$key] ["type"] == "time") {
								$date = $value;
								if(is_array($date) && array_key_exists("date", $date)){
									$value = new \DateTime ( $date["date"] );
								}elseif(is_string($date)){
									$value = new \DateTime ( $date );
								}else{
									$value = new \DateTime ();
								}
					}

					if (method_exists ( $newClass, $setter )) {
						// if(is_bool($value) && !$value) $value = "false";
						// elseif(is_bool($value) && $value) $value = "true";
						// echo $setter." - ".$value."\n";
						// echo $key."\n";
						// echo $newClass->$getter()." - ".$value."\n";
						if($newClass->$getter() != "" && $newClass->$getter() != null && $newClass->$getter() != $value)
						{
							$this->setPropertyChanges($key, $newClass->$getter(), $value, $data['id'], 'PUT');
							
						}
						elseif($newClass->$getter() == "" || is_null($newClass->$getter()))
						{
							if($fieldMapping [$key] ["type"] == "datetime" || $fieldMapping [$key] ["type"] == "date" || $fieldMapping [$key] ["type"] == "time"){
								// print_r($value->format('Y-m-d H'));
								$val = $value->format('Y-m-d H:i:s');
								$this->setPropertyChanges($key, null, $val, null, 'POST');
							}
							else{
								$this->setPropertyChanges($key, null, $value, null, 'POST');
							}
						}
						$newClass->$setter ( $value );
						// $this->setPropertyChanges($key, $oldValue, $value, $entityId, $action)
					}
				}
			}
			elseif(empty($value))
			{
				if(array_key_exists($key, $associations)){
					$asso = $associations[$key];
					if($asso ["type"] == 4 || $asso ["type"] == 8){
						// echo $key;
						$getter = 'get' . ucwords ( $key );
						
						$collection = $newClass->$getter();

						if(is_object($collection)){
							if($collection and $collection->count() > 0){
								// echo count($obj);
								// $foundInCollection = $this->lookInCollection ( $obj, $collection);
								$toDelete = $this->removeNotBe(null, $collection);

								if (substr ( $asso ["fieldName"], - 1 ) == "s")
									$removeMethod = 'remove' . ucwords ( substr ( $key, 0, - 1 ) );
								else
									$removeMethod = 'remove' . ucwords ( $key );
								// print_r($toDelete);
								foreach ($toDelete as $key => $ent) {

									$newClass->$removeMethod($ent);
									if($asso ["type"] == 4){
										$em->remove($ent);
									}
								}
							}
						}
					}
					else{
						$setter = 'set' . ucwords ( $key );
						// $newClass->$setter(null);
					}
				}
			}
		}
		if($persist)
		{
			$em->persist($newClass);
		}
		// echo get_class($newClass)."\n";
		// echo $newClass->getId();
		// $this->setInfoToLog($class);
		return $newClass;
	}

	public function setInfoToLog($entityName, $entityPost = null, $entityDeleted = null){
        $request = $this->container->get("request");
        $method = $request->getMethod();
        $logService = $this->container->get("transactionLog");
        $user = $this->container->get('security.context')->getToken()->getUser();
        switch($method){
            case 'POST':
                // $data = $this->container->get("talker")->getData();
            	$contentType = $request->headers->get('content_type');
		        $explode = explode(";", $contentType);
		        $contentType = $explode[0];
            	$action = $request->headers->get('X-ACCION');

				if($action){
		         	if($contentType == 'multipart/form-data'){
		         		$id = $request->get('id');
			        	if($action == 'update'){
							$logService->write($entityName, null, $user->getId(), "POST", $id);
			        	}
			        }
		        }else{
		        	$logService->write($entityName, $this->propertyChanged, $user->getId(), "POST", $entityPost->getId());
		        }
                break;
            case 'PUT':
            	if(count($this->propertyChanged) > 0){
            		// echo "<br/> id: ".$this->propertyChanged[0]['entityId'];
                	$logService->write($entityName, $this->propertyChanged, $user->getId(), "PUT", $this->propertyChanged[0]['entityId']);
                	
                }
                break;
            case "DELETE":
                if($entityDeleted)
                    $logService->write($entityName, null, $user->getId(), "DELETE", $entityDeleted->getId());
                break;
        }
    }
	public function hasChanges($data, $class)
	{
		$em = $this->em;
		$metadata = $em->getClassMetadata($class);
		// print_r($metadata);
		$obj = $em->getRepository($class)->getEntity($data[$metadata->identifier[0]]);
		foreach($data as $key => $value)
		{
			if(in_array($key, $this->ignoreProperties)) continue;
			if(array_key_exists($key, $metadata->fieldMappings))
			{
				$getter = 'get'.ucwords($key);
				// echo $key.' - '.$class."\n";
				if($value != $obj->$getter())
				{
					return true;
					break;
				}
			}
			elseif(array_key_exists($key, $metadata->associationMappings))
			{

				if(!is_null($value)){
					if($metadata->associationMappings[$key]['type'] == 4 || $metadata->associationMappings[$key]['type'] == 8)
					{
					}
					else
					{
						if(is_array($value)){
							$getter = 'get'.ucwords($key);
							// echo $class ." - ".$key."\n";
							// print_r($value);
							// echo $obj->$getter()->getId()."\n";
							// echo $key."\n";
							// print_r($value);
							if(array_key_exists('id', $value))
							{
								if($obj->$getter()->getId() != $value['id'])
								{
									return true;
								}
							}
							else
							{
								return true;
							}
						}
					}
				}

			}
		}
		// echo "false\n\n";
		return false;
	}

	private function removeNotBe($entity = null, &$coll){
		$em =  $this->em;
		$toDelete = array();
		$items = null;
		if(is_array($coll)){
			$items = $coll;
		}else{
			$items = $coll->toArray();
		}
		
		foreach ($items as $key => $obj) {
			if(!is_null($entity))
			{
				if(is_array($entity)){
					$found = false;
					foreach ($entity as $ent) {
						if($obj->getId() == $ent->getId()){
							$found=true;
							break;
						}
					}
					if(!$found){
						$toDelete[$key] = $obj;
					}
				}else{
					if($obj->getId() != $entity->getId()){
						$toDelete[$key] = $obj;
					}
				}
			}
			else{
				$toDelete[$key] = $obj;
			}
		}
		 
		/*foreach ($toDelete as $key => $obj) {
		 $coll->removeElement($obj);
		 //$em->remove($obj);
		 }*/
		return $toDelete;
	}

	private function lookInCollection(&$entity, $coll){
		$em =  $this->em;
		if(is_array($entity)){
			$e = array();
			foreach ($entity as $o) {
				$e[$o->getId()] = $this->lookInCollection($o, $coll);
			}
			return $e;
		}else{
			$count = 0;
			if(is_array($coll)){
				$count = count($coll);
			}else{
				$count = $coll->count();
			}
			if($count > 0 && !is_null($entity->getId())){
				//echo "<br>Voy a buscar ".$entity->getId();
				$found = false;
				//print_r($coll);
				$coll = is_array($coll)?$coll:$coll->toArray();
				foreach ($coll as $item){
					if($item->getId() == $entity->getId()){
						$found = true;
						break;
					}
				}
				//echo $found ? "true" : "false";
				return $found;
				//$item = $coll->get($entity->getId());
				 
				 
				//echo is_null($item) ? " Encontrado " : " No encontrado ";
				 
				/*if(!is_null($item)){
				 $em = $this->em;
				 $class = get_class($entity);
				 $metadata = $em->getClassMetadata($class);
				 $associations = $metadata->associationMappings;
				 $fieldMapping = $metadata->fieldMappings;
				 foreach ($fieldMapping as $field) {
				 if(is_array($field)){
				 if(array_key_exists("fieldName", $field)){
				 $setter = "set" . ucwords($field["fieldName"]);
				 }
				 }else{
				 $setter = "set" . ucwords($field);
				 }
				 }
				 }*/
			}
			return false;
		}
	}

}
?>