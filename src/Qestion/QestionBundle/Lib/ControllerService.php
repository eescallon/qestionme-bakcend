<?php
namespace Qestion\QestionBundle\Lib;
use Symfony\Component\Process\Exception\RuntimeException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class ControllerService
{
	private $propertyChanged = array();
    private $em;
    private $co;
    
    private $myBundles = array(
        "Qestion"
    );
    // protected $nameSpace = "AYS\ArchiveBundle\Entity\\";
    public function __construct($co) {
        $this->em = $co->get('doctrine')->getManager();
        $this->co = $co;
    }
    /**
     * Funcion para colocar entre comillas el valor de una variable booleana
     */
    public function checkBoolean(&$Entity, $className)
    {
        $md = $this->em->getClassMetadata($className);
        $associations = $md->associationMappings;
        $fieldMapping = $md->fieldMappings;
        foreach($fieldMapping as $key => $value)
        {
            if($value['type'] == "boolean")
            {
                // echo $key;
                $val = $Entity->{'get'. ucfirst($key)}();
                if($val == true)
                {
                    // echo "hola1";
                    $Entity->{'set'. ucfirst($key)}("true");
                }
                else
                {
                    // echo "hola2";
                    $Entity->{'set'. ucfirst($key)}("false");
                }
            }
        }
    }
	
	public function setPropertyChanges($propertyName, $oldValue, $newValue, $entityId, $action){
		
		$this->propertyChanged[] = array(
			"propertyName" => $propertyName,
			"oldValue" => $oldValue,
			"newValue" => $newValue,
			"entityId" => $entityId,
			'action' => $action
		);
	}
	
	public function getRequestEntity($className, $id = null, $document = false){
		$request = $this->co->get("request");
		$contentType = $request->headers->get('content_type');
		// print_r($contentType);
		$explode = explode(";", $contentType);
		$contentType = $explode[0];		
		if($document)
		{
			switch ($contentType) {
				case 'application/x-www-form-urlencoded':
					return $this->applicationFormDocument($className, $id);
					break;
				case 'multipart/form-data':
					return $this->applicationFormDocument($className, $id);
					break;
						
				default:
					return $this->applicationJsonDocument($className, $id);
					break;
			}
		}
		else{
			switch ($contentType) {
				case 'application/x-www-form-urlencoded':
					return $this->applicationForm($className, $id);
					break;
				case 'multipart/form-data':
					return $this->applicationForm($className, $id);
					break;
				default:
					return $this->applicationJson($className, $id);
					break;
			}
		}
	}
	
	private function applicationForm($className, $id = null){
				
		$request = $this->co->get("request");
		$reflectionClass = new \ReflectionClass($className);
		$metadata = $this->em->getClassMetadata($className);
		$associations = $metadata->associationMappings;
		//print_r($associations);
		$fieldMapping = $metadata->fieldMappings;
		$newClass = new $className();
		//echo "\n\nRequest\n\n";
		// echo $request;
		// echo "\n\n---------------- defaultCompany--------------------\n\n";
		// echo $request->get('defaultCompany');
		if($id){
			$entity = $this->em->getRepository($className)->find($id);
			
			// $data = array();
			// foreach ($fieldMapping as $key => $value) {
			// 	// echo $key;
			// 	// print_r($value);
			// 	$val = $request->get($key);
			// 	$data[$key] = $val;
			// 	// echo "\nvalue ".$value."..........................................\n\n";
			// 	// $setter = "set" . ucwords($prop->getName());
			// 	// echo "\nsetter ".$setter."..........................................\n\n";
			// }
			// foreach ($associations as $key => $value) {
			// 	// echo $key;
			// 	// print_r($value);
			// 	if($value['type'] == 4 || $value['type'] == 8){
					
			// 	}
			// 	$val = $request->get($key);
			// 	$data[$key] = $val;
				
			// 	// echo "\nvalue ".$value."..........................................\n\n";
			// 	// $setter = "set" . ucwords($prop->getName());
			// 	// echo "\nsetter ".$setter."..........................................\n\n";
			// }
			// print_r($data);
			return $entity;
		}else{
			do {
				$props = $reflectionClass->getProperties();
				foreach ($props as $prop) {
					//echo "\n<br/>".$prop->getName();
					
					$value = $request->get($prop->getName());
					//echo "\nvalue ".$value."..........................................\n\n";
					$setter = "set" . ucwords($prop->getName());
					if(!is_null($value)){
						if (array_key_exists($prop->getName(), $associations)) {
							$asso = $associations[$prop->getName()];
							//print_r($asso);
							$entity = $asso["targetEntity"];
							// print_r($entity);
							// echo "\n".$value;
							$obj = $this->em->getRepository($entity)->find($value);
							// print_r($obj);
							$value = $obj;
							if($asso["type"] == 4 || $asso["type"] == 8){
								if (substr($prop->getName(), -1) == "s")
									$setter = 'add' . ucwords(substr($prop->getName(), 0, -1));
								else
									$setter = 'add' . ucwords($prop->getName());
							}
						}elseif (array_key_exists($prop->getName(), $fieldMapping)) {
							if ($fieldMapping[$prop->getName()]["type"] == "datetime") {
								$date = $value;
								$value = new \DateTime($date);
							}
						}
						if(method_exists($newClass, $setter))
							$newClass->$setter($value);
					}
				}
				$reflectionClass = $reflectionClass->getParentClass();
			} while (false !== $reflectionClass);
			
			return $newClass;
		}
	}
	/**
	 * Funcion para cuando es un documento
	 */
	private function applicationJsonDocument($className, $id = null){
		$request = $this->co->get("request");
		$data = $request->getContent();
		if(empty($data) || !$data)
			throw new \Exception("No data for update");
		$data = json_decode($data, true);
		$dm = $this->co->get('doctrine_mongodb')->getManager();
		$md = $dm->getClassMetadata($className);
		if(!is_null($id)){			
			$idField = $md->identifier[0];
			$data[$idField] = $id;
		}
		$newClass = $this->co->get("talker")->denormalizeDocument($className, $data);
		return $newClass;
	}
	
	/**
	 * Funcion para cuando es una entidad
	 */
	private function applicationJson($className, $id = null){
		$request = $this->co->get("request");
		$data = $request->getContent();
		if(empty($data) || !$data)
			throw new \Exception("No data for update");
		$data = json_decode($data, true);
		$metadata = $this->em->getClassMetadata($className);
        $method = $request->getMethod();
        if($method == "PUT")
        {        	
        	$entity = $this->em->getRepository($className)->find($id);
			$associations = $metadata->associationMappings;
			$associationsName = [];
			foreach($associations as $name => $value){
				$associationsName[] = $name;
			}
			//print_r($associationsName);
        	foreach($data as $key => $value)
			{
				// print_r($value);
				if(array_key_exists($key, $metadata->reflFields))
				{
					//print_r($entity)
					$getter = "get". ucwords($key);
					$entityValue = $entity->$getter();
					$ent;
					$v;
					$sw = true;
					if(is_object($entityValue))
					{
						if(get_class($entityValue) == "DateTime")
						{							
							$entityValue = $entityValue->format('Y-m-d');
							$this->setPropertyChanges($key, $entityValue, $value, $id, 'PUT');
						}
						elseif (get_class($entityValue) == "Doctrine\ORM\PersistentCollection") {
							// $changes = $this->associationHasChanges($value, $entityValue);
							// // print_r($changes);
							// foreach($changes as $change)
							// {
							// 	if(is_object($change['data']))
							// 	{
							// 		$d = $change['data'];
							// 	}
							// 	else{
							// 		$d = $this->em->getRepository('AYS\UserSecurityBundle\Entity\Permission')->getArrayEntityWithOneLevel(array('id' => $change['data']['id']));
							// 	}
								
							// 	if($change['action'] == 'DELETE')
							// 	{
							// 		$this->setPropertyChanges($key, $d->getId(), 'null', $id, $change['action']);
							// 	}
							// 	else{
							// 		$this->setPropertyChanges($key, 'null', $d->getName(), $id, $change['action']);
							// 	}
							// }
						}
						else{
							if(array_key_exists('id', $value))
							{
								if($entityValue->getId() != $value['id'])
								{
									$this->setPropertyChanges($key, $entityValue->getName(), $value['id'], $id, 'PUT');
								}
							}
							
						}
					}
					else{
						
						if($value != $entityValue)
						{
							$this->setPropertyChanges($key, $entityValue, $value, $id, 'PUT');
						}
					}
				}
			}
        }
		$keys = array_keys($data);
		if(is_numeric($keys[0]))
		{
			$newClass = array();
			foreach($data as $value)
			{
				if(!is_null($id)){
					$idField = $metadata->identifier[0];
					$value[$idField] = $id;
				}
				$newClass[] = $this->co->get("talker")->denormalizeEntity($className, $value);
			}
			return $newClass;
		}
		else
		{
			if(!is_null($id)){
				$idField = $metadata->identifier[0];
				$data[$idField] = $id;
			}
			$newClass = $this->co->get("talker")->denormalizeEntity($className, $data);
			return $newClass;
		}
	}
	private function associationHasChanges($data, $oldData)
	{
		$return = array();
		$founded = array();
		foreach($oldData as $key => $value)
		{
			foreach($data as $k => $v)
			{
				if($v['id'] == $value->getId())
				{
					$founded[$v['id']] = $v;
					break;
				}
			}
		}
		foreach($oldData as $key => $value)
		{
			if(array_key_exists($value->getId(), $founded))
			{
				continue;
			}
			else{
				$return[$key] = array(
					"action" => 'DELETE',
					'data' => $value
				);
			}
		}
		foreach($data as $key => $value)
		{
			if(array_key_exists('id', $value))
			{
				if(array_key_exists($value['id'], $founded))
				{
					continue;
				}
				else{
					$return[$key] = array(
						"action" => 'POST',
						'data' => $value
					);
				}
			}
			else{
				$return[$key] = array(
					"action" => 'POST',
					'data' => $value
				);
			}
			
		}
		return $return;
	}
	
	// +++++++++++++++++++ SAVE +++++++++++++++++++++++++++++
	public function doSave(&$entity) {
		
		if(is_array($entity)){
			foreach($entity as $ent){
				$this->em->persist($ent);
			}
		}else{
			$this->em->persist($entity);
		}
		$validator = $this->co->get("validator");
		$errors = $validator->validate($entity);
		if (count($errors) > 0) {
			//aparece mensaje y array
			// echo $errors->__toString();
			$explode = explode("\n", $errors);
			//aparece array y el mensaje 
			//print_r($explode);
			throw new RuntimeException($errors->__toString());
		}
		// echo $entity->getCompany()->getId();
		$this->em->flush();
		// echo "entre";
		return $entity;
	}
	
	// +++++++++++++++++++ UPDATE +++++++++++++++++++++++++++++
	public function doUpdate(&$entity) {
		$validator = $this->co->get("validator");
		$errors = $validator->validate($entity);
		if (count($errors) > 0) {
			throw new RuntimeException($errors->__toString());
		}
		$this->em->flush();
		//echo $entity;
		return $entity;
	}
	// +++++++++++++++++++ DELETE +++++++++++++++++++++++++++++
	public function doDelete($entity) {
		if(is_array($entity)){
			foreach($entity as $ent){
				$ent->setDeleted(true);
			}
		}else{
			$entity->setDeleted(true);
		}
		$this->em->flush();
	}
	
	public function applyPagination(&$data, $count, $start, $limit) {
		$paginateData = array ();
		for($i = $start; $i < ($start + $limit); $i ++) {
			if ($count > $i) {
				$paginateData [] = $data [$i];
			}
		}
		$data = $paginateData;
		return $data;
	}
	
	public function generatePassword($l) {
		$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
		$pass = "";
		for($i = 0; $i < $l; $i ++) {
			$n = rand ( 0, strlen ( $alphabet ) - 1 );
			$pass .= $alphabet [$n];
		}
		return $pass;
	}
	// public function setInfoToLog($entityName, $entityPost, $entityDeleted = null){
 //        $request = $this->co->get("request");
 //        $method = $request->getMethod();
 //        $logService = $this->co->get("transactionLog");
 //        $user = $this->co->get('security.context')->getToken()->getUser();
 //        switch($method){
 //            case 'POST':
 //                // $data = $this->co->get("talker")->getData();
 //            	$contentType = $request->headers->get('content_type');
	// 	        $explode = explode(";", $contentType);
	// 	        $contentType = $explode[0];
 //            	$action = $request->headers->get('X-ACCION');
	// 			if($action){
	// 	         	if($contentType == 'multipart/form-data'){
	// 	         		$id = $request->get('id');
	// 		        	if($action == 'update'){
	// 						$logService->write($entityName, null, $user->getId(), "POST", $id);
	// 		        	}
	// 		        }
	// 	        }else{
	// 	        	$logService->write($entityName, null, $user->getId(), "POST", $entityPost->getId());
	// 	        }
 //                break;
 //            case 'PUT':
 //            	if(count($this->propertyChanged) > 0){
 //            		// echo "<br/> id: ".$this->propertyChanged[0]['entityId'];
 //                	$logService->write($entityName, $this->propertyChanged, $user->getId(), "PUT", $this->propertyChanged[0]['entityId']);
                	
 //                }
 //                break;
 //            case "DELETE":
 //                if($entityDeleted)
 //                    $logService->write($entityName, null, $user->getId(), "DELETE", $entityDeleted->getId());
 //                break;
 //        }
 //    }
}