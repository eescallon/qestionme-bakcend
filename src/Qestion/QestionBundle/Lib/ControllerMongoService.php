<?php
namespace Qestion\QestionBundle\Lib;
use Symfony\Component\Process\Exception\RuntimeException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
class ControllerMongoService
{
	private $propertyChanged = array();
    private $co;
    private $dm;
    
    private $myBundles = array(
        "Qestion"
    );
    // protected $nameSpace = "AYS\ArchiveBundle\Entity\\";
    public function __construct($co) {
        $this->co = $co;
        $this->dm = $co->get('doctrine_mongodb')->getManager();
    }
    /**
     * Funcion para colocar entre comillas el valor de una variable booleana
     */
    public function checkBoolean(&$Entity, $className)
    {
        $md = $this->dm->getClassMetadata($className);
        $associations = $md->associationMappings;
        $fieldMapping = $md->fieldMappings;
        foreach($fieldMapping as $key => $value)
        {
            if($value['type'] == "boolean")
            {
                // echo $key;
                $val = $Entity->{'get'. ucfirst($key)}();
                if($val == true)
                {
                    // echo "hola1";
                    $Entity->{'set'. ucfirst($key)}("true");
                }
                else
                {
                    // echo "hola2";
                    $Entity->{'set'. ucfirst($key)}("false");
                }
            }
        }
    }
	
	public function setPropertyChanges($propertyName, $oldValue, $newValue, $entityId, $action){
		
		$this->propertyChanged[] = array(
			"propertyName" => $propertyName,
			"oldValue" => $oldValue,
			"newValue" => $newValue,
			"entityId" => $entityId,
			'action' => $action
		);
	}
	
	public function getRequestDocument($className, $id = null){
		$request = $this->co->get("request");
		$contentType = $request->headers->get('content_type');
		$explode = explode(";", $contentType);
		$contentType = $explode[0];
				
		switch ($contentType) {
			case 'application/x-www-form-urlencoded':
				return $this->applicationForm($className, $id);
				break;
			case 'multipart/form-data':
				return $this->applicationForm($className, $id);
				break;
			default:
				return $this->applicationJson($className, $id);
				break;
		}
	}
	
	private function applicationForm($className, $id = null){
				
		$request = $this->co->get("request");
		$reflectionClass = new \ReflectionClass($className);
		$metadata = $this->dm->getClassMetadata($className);
		$associations = $metadata->associationMappings;
		$fieldMapping = $metadata->fieldMappings;
		$newClass = new $className();
		if($id){
			$entity = $this->dm->getRepository($className)->find($id);
			return $entity;
		}else{
			do {
				$props = $reflectionClass->getProperties();
				foreach ($props as $prop) {
					//echo "\n<br/>".$prop->getName();
					
					$value = $request->get($prop->getName());
					//echo "\nvalue ".$value."..........................................\n\n"
					$setter = "set" . ucwords($prop->getName());
					if(!is_null($value)){
						if (array_key_exists($prop->getName(), $associations)) {
							$asso = $associations[$prop->getName()];
							//print_r($asso);
							$entity = $asso["targetEntity"];
							// print_r($entity);
							// echo "\n".$value;
							$obj = $this->em->getRepository($entity)->find($value);
							// print_r($obj);
							$value = $obj;
							if($asso["type"] == 4 || $asso["type"] == 8){
								if (substr($prop->getName(), -1) == "s")
									$setter = 'add' . ucwords(substr($prop->getName(), 0, -1));
								else
									$setter = 'add' . ucwords($prop->getName());
							}
						}elseif (array_key_exists($prop->getName(), $fieldMapping)) {
							if ($fieldMapping[$prop->getName()]["type"] == "datetime") {
								$date = $value;
								$value = new \DateTime($date);
							}
						}
						if(method_exists($newClass, $setter))
							$newClass->$setter($value);
					}
				}
				$reflectionClass = $reflectionClass->getParentClass();
			} while (false !== $reflectionClass);
			
			return $newClass;
		}
	}
	/**
	 * Funcion para cuando es un documento
	 */
	private function applicationJson($className, $id = null){
		$request = $this->co->get("request");
		$data = $request->getContent();
		if(empty($data) || !$data)
			throw new \Exception("No data for update");
		$data = json_decode($data, true);
		$dm = $this->dm;
		$md = $dm->getClassMetadata($className);
		if(!is_null($id)){			
			$idField = $md->identifier[0];
			$data[$idField] = $id;
		}
		$newClass = $this->co->get("talker")->denormalizeDocument($className, $data);
		return $newClass;
	}
	private function associationHasChanges($data, $oldData)
	{
		$return = array();
		$founded = array();
		foreach($oldData as $key => $value)
		{
			foreach($data as $k => $v)
			{
				if($v['id'] == $value->getId())
				{
					$founded[$v['id']] = $v;
					break;
				}
			}
		}
		foreach($oldData as $key => $value)
		{
			if(array_key_exists($value->getId(), $founded))
			{
				continue;
			}
			else{
				$return[$key] = array(
					"action" => 'DELETE',
					'data' => $value
				);
			}
		}
		foreach($data as $key => $value)
		{
			if(array_key_exists('id', $value))
			{
				if(array_key_exists($value['id'], $founded))
				{
					continue;
				}
				else{
					$return[$key] = array(
						"action" => 'POST',
						'data' => $value
					);
				}
			}
			else{
				$return[$key] = array(
					"action" => 'POST',
					'data' => $value
				);
			}
			
		}
		return $return;
	}
	
	// +++++++++++++++++++ SAVE +++++++++++++++++++++++++++++
	public function doSave(&$document) {
		if(is_array($document)){
			foreach($document as $ent){
				$this->dm->persist($ent);
			}
		}else{
			$this->dm->persist($document);
		}
		$validator = $this->co->get("validator");
		$errors = $validator->validate($document);
		if (count($errors) > 0) {
			throw new RuntimeException($errors->__toString());
		}
		// echo $document->getCompany()->getId();
		$this->dm->flush();
		
		return $document;
	}
	
	// +++++++++++++++++++ UPDATE +++++++++++++++++++++++++++++
	public function doUpdate(&$document) {
		$validator = $this->co->get("validator");
		$errors = $validator->validate($document);
		if (count($errors) > 0) {
			throw new RuntimeException($errors->__toString());
		}
		$this->dm->flush();
		//echo $document;
		return $document;
	}
	// +++++++++++++++++++ DELETE +++++++++++++++++++++++++++++
	public function doDelete($document) {
		if(is_array($document)){
			foreach($document as $ent){
				$ent->setDeleted(true);
			}
		}else{
			$document->setDeleted(true);
		}
		$this->dm->flush();
	}
	
	public function applyPagination(&$data, $count, $start, $limit) {
		$paginateData = array ();
		for($i = $start; $i < ($start + $limit); $i ++) {
			if ($count > $i) {
				$paginateData [] = $data [$i];
			}
		}
		$data = $paginateData;
		return $data;
	}
	
	public function generatePassword($l) {
		$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
		$pass = "";
		for($i = 0; $i < $l; $i ++) {
			$n = rand ( 0, strlen ( $alphabet ) - 1 );
			$pass .= $alphabet [$n];
		}
		return $pass;
	}
	// public function setInfoToLog($entityName, $entityPost, $entityDeleted = null){
 //        $request = $this->co->get("request");
 //        $method = $request->getMethod();
 //        $logService = $this->co->get("transactionLog");
 //        $user = $this->co->get('security.context')->getToken()->getUser();
 //        switch($method){
 //            case 'POST':
 //                // $data = $this->co->get("talker")->getData();
 //            	$contentType = $request->headers->get('content_type');
	// 	        $explode = explode(";", $contentType);
	// 	        $contentType = $explode[0];
 //            	$action = $request->headers->get('X-ACCION');
	// 			if($action){
	// 	         	if($contentType == 'multipart/form-data'){
	// 	         		$id = $request->get('id');
	// 		        	if($action == 'update'){
	// 						$logService->write($entityName, null, $user->getId(), "POST", $id);
	// 		        	}
	// 		        }
	// 	        }else{
	// 	        	$logService->write($entityName, null, $user->getId(), "POST", $entityPost->getId());
	// 	        }
 //                break;
 //            case 'PUT':
 //            	if(count($this->propertyChanged) > 0){
 //            		// echo "<br/> id: ".$this->propertyChanged[0]['entityId'];
 //                	$logService->write($entityName, $this->propertyChanged, $user->getId(), "PUT", $this->propertyChanged[0]['entityId']);
                	
 //                }
 //                break;
 //            case "DELETE":
 //                if($entityDeleted)
 //                    $logService->write($entityName, null, $user->getId(), "DELETE", $entityDeleted->getId());
 //                break;
 //        }
 //    }
}