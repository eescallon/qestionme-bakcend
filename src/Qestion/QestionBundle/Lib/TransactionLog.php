<?php
namespace Qestion\QestionBundle\Service;

use Qestion\QestionBundle\Document\Transaction;
use Qestion\QestionBundle\Document\User;
use Qestion\QestionBundle\Document\ValueChange;

class TransactionLog
{
	private $dm;
    private $co;

	public function __construct($co)
    {
        $this->co = $co;
        $this->dm = $co->get('doctrine_mongodb')->getManager();
	}

	public function write($entity, array $values = null, $userId, $action, $entityId)
	{
        $userData = $this->co->get("doctrine")->getmanager()->getRepository("ShoppingBundle:User")->findUserById($userId);
        $userData = $userData[0];
        $user = new User();
        $user->setUserId($userData['id'])
            ->setName($userData['person']['firstname'])
            ->setLastname($userData['person']['lastname'])
            ->setEmail($userData['email'])
            ->setPhoto($userData['person']['photo']);
		$transaction = new Transaction();
		$transaction->setInsertDate(new \MongoDate());

        // echo "<br/><br/>".$entity;
        // echo "<br/>".$entityId;
        // echo "<br/><br/>";
    	$transaction->setEntity($entity);
    	$transaction->setUser($user);
    	$transaction->setAction($action);
    	$transaction->setEntityId($entityId);

        if(!is_null($values))
        {
            foreach($values as $key => $value)
            {

                $valueChange = new ValueChange();
                if(!is_object($value['oldValue'])){
                    $valueChange->setOldValue($value['oldValue']);
                }
                if(!is_object($value['newValue']))
                {
                    $valueChange->setNewValue($value['newValue']);
                }
                $valueChange->setAttribute($value['propertyName']);
                $valueChange->setAction($value['action']);
                $transaction->addValueChange($valueChange);
            }
        }
        $this->dm->persist($transaction);
        $this->dm->flush();
        // echo "fdasfda";
        try
        {
        }
        catch(\Exception $e)
        {
        }
    	return $transaction;
	}
}
?>