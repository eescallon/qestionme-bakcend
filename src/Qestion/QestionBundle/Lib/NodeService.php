<?php
namespace Qestion\QestionBundle\Lib;
use Symfony\Component\Yaml\Parser;
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
class NodeService
{
	private $co;
	public function __construct($co)
	{
		$this->co = $co;
	}
	public function sendMessage($event, $message)
	{
		$serverNode = $this->co->getParameter('node_config');
		$client = new Client(new Version1X($serverNode));
		$client->initialize();
		$client->emit($event, $message);
		$client->close();
	}	
}