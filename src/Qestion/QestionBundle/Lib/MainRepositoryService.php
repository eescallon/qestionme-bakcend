<?php
namespace Qestion\QestionBundle\Lib;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Serializer;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Tools\Pagination\Paginator;
class MainRepositoryService {
    private $em;
    private $container;
    public function __construct($co) {
        $this->em = $co->get('doctrine')->getManager();
        $this->container = $co;
    }
    public function fixCriteria(array $criteria, $entityName) {
        //Unless explicitly requested to return deleted items, we want to return non-deleted items by default
        $md = $this->em->getClassMetadata($entityName);
        $fieldMapping = $md->fieldMappings;
        if (array_key_exists('deleted', $fieldMapping)) {
            if (!in_array('deleted', $criteria)) {
                $criteria['deleted'] = false;
            }
        }
        return $criteria;
    }
    public function getArrayBy(array $criteria, array $orderBy = null, $sresult = null, $limit = null, $orderType = null) {
        $query = $this->getSQLStructure($criteria, $orderBy, $sresult, $limit, $entityName, null, $orderType);
        $data = $query->getQuery()->getArrayResult();
        $count = count($data);
        if(!is_null($sresult) && !is_null($limit))
        {
            $paginateData = array();
            for($i=$sresult;$i<($sresult+$limit);$i++)
            {
                if($count > $i)
                {
                    $paginateData[] = $data[$i];
                }
            }
            $data = $paginateData;
        }
        return array(
            "total" => $count,
            "data" => $data
        );
    }
    private function getSQLStructure(array $criteria, $orderBy = null, $sresult = null, $limit = null, $filter = null, $entityName, $ids = null, $orderType = null)
    {
        $criteria = $this->fixCriteria($criteria, $entityName);
        $persister = $this->em->getUnitOfWork()
                ->getEntityPersister($entityName);
        $query = $this->em->getRepository($entityName)->createQueryBuilder('z');
        $sequence = array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
            "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "v", "w", "x", "y");
        $md = $this->em->getClassMetadata($entityName);
        $fieldMapping = $md->fieldMappings;
        $associations = $md->associationMappings;
        $i = 0;
        $select = "z";
        $entities = array();
        foreach ($associations as $name => $value) {
            $select.=", " . $sequence[$i];
         
            $cm = $this->em->getClassMetadata($value['targetEntity']);
            $fmapping = $cm->fieldMappings;
            if(isset($fmapping['deleted'])){
                $query->leftJoin('z.' . $name, $sequence[$i], "WITH", $sequence[$i].".deleted = FALSE");
            }else{
                $query->leftJoin('z.' . $name, $sequence[$i]);
            }
            $entities[$name] = $sequence[$i];
            $i++;
        }
        $query->select($select);
        if (count($criteria) > 0) {
            $str_where = "";
            $i = 0;
            foreach ($criteria as $name => $value) {
                if ($i > 0)
                    $str_where .= " AND ";
                if(array_key_exists($name, $associations))
                {
                    if($associations[$name]["type"] == 8)
                    {
                        $entityName = $entities[$name];
                        $str_where .= $entityName.".id = :$name";
                    }
                    else
                    {
                        $str_where .= "z.$name = :$name";
                    }
                }
                else
                {
                    $str_where .= "z.$name = :$name";
                }
                $i++;
            }
            $query->where($str_where);
            $i = 0;
            foreach ($criteria as $name => $value) {
                if (isset(
                                $persister->getClassMetadata()->fieldMappings[$name]))
                    $query
                            ->setParameter($name, $value, Type::getType(
                                            $persister->getClassMetadata()
                                            ->fieldMappings[$name]['type'])
                                    ->getBindingType());
                else
                    $query->setParameter($name, $value, Type::INTEGER);
                $i++;
            }
        }
        // print_r($query->getQuery()->getArrayResult());
        if(!is_null($ids))
        {
            // print_r($ids);
            $query->andWhere('z.id IN (:ids)');
            $query->setParameter('ids', $ids);
        }
        if (!is_null($orderBy)) {
            switch ($orderType) {
                case 'asc':
                    $query->orderBy("z.$orderBy", "ASC");
                    break;
                case 'desc':
                    $query->orderBy("z.$orderBy", "DESC");
                    break;
                
                default:
                    $query->orderBy("z.$orderBy");
                    break;
            }
        }
        // print_r($query->getQuery()->getArrayResult());
        if($filter)
        {
            $counter = 1;
            foreach($filter as $fil)
            {
                $fil['value'] = trim($fil['value']);
                if(!is_null($fil['value']) && $fil['value'] != "")
                {
                    $this->setFilterToQuery($query, $fil, $counter);
                    $counter++;
                }
                
            }
            
        }
        return $query;
    }
    private function getSQLStructureWithoutLevel($id = null, $entityName, $orderBy = null, $filter = null, $relations = null, $ids = null, $orderType = null )
    {
        $query = $this->em->getRepository($entityName)->createQueryBuilder('z');
        $sequence = array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
            "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "v", "w", "x", "y");
        $md = $this->em->getClassMetadata($entityName);
        $fieldMapping = $md->fieldMappings;
        $associations = $md->associationMappings;
        $select = "z";
        
        $where = "";
        if(array_key_exists('deleted', $fieldMapping)){
            $where .= "z.deleted = false";
            $query->where($where);
        }
        if(!is_null($relations))
        {
            $i = 0;
            foreach($relations as $relation)
            {
                if(array_key_exists($relation, $associations))
                {
                    $select.=", " . $sequence[$i];
                    $cm = $this->em->getClassMetadata($associations[$relation]['targetEntity']);
                    $fmapping = $cm->fieldMappings;
                    if(isset($fmapping['deleted'])){
                        $query->leftJoin('z.' . $relation, $sequence[$i], "WITH", $sequence[$i].".deleted = FALSE");
                    }else{
                        $query->leftJoin('z.' . $relation, $sequence[$i]);
                    }
                    $i++;
                }
            }
        }
        $query->select($select);
        if(!is_null($ids))
        {
            // print_r($ids);
            $query->andWhere('z.id IN (:ids)');
            $query->setParameter('ids', $ids);
        }
        if (!is_null($orderBy)) {
            switch ($orderType) {
                case 'asc':
                    $query->orderBy("z.$orderBy", "ASC");
                    break;
                case 'desc':
                    $query->orderBy("z.$orderBy", "DESC");
                    break;
                
                default:
                    $query->orderBy("z.$orderBy");
                    break;
            }
            
        }
        
        if(!is_null($id)){
            $query->andWhere('z.id = :id');
            $query->setParameter('id', $id);
        }
        if(!is_null($filter))
        {
            $counter = 1;
            foreach($filter as $fil)
            {
                $this->setFilterToQuery($query, $fil, $counter);
                $counter++;
            }
        }
        
        return $query;
    }
    public function getEntity($id = null, $entityName, $orderBy = null, $filter = null, $skip = null, $pageSize = null, $relations = null, $orderType = null)
    {
        // echo $orderType."\n";
        try{
            $query = $this->getSQLStructureWithoutLevel($id, $entityName, $orderBy, $filter, $relations, null, $orderType);
        }
        catch(DBALException $e)
        {
            throw new \Exception("Error Processing Request", 1);
        }
        $count = null;
        if(!is_null($skip) && !is_null($pageSize)){
            $count = $this->countTotal($query);
            $first = $this->getFirstEntities($query, $skip, $pageSize);
            $ids = array();
            foreach($first->getQuery()->getArrayResult() as $value)
            {
                $ids[] = $value['id'];
            }
            try{
                $query = $this->getSQLStructureWithoutLevel($id, $entityName, $orderBy, $filter, $relations, $ids, $orderType);
            }
            catch(DBALException $e)
            {
                throw new \Exception("Error Processing Request", 1);
            }
        }
        if(!is_null($id)){
            $data = $query->getQuery()->getSingleResult();
            return $data;
        }else{
            $data = $query->getQuery()->getArrayResult();
            if(is_null($count)){
                $count = count($data);
            }
            return array(
                "total" => $count,
                "data" => $data
            );
            
        }
        
    }
    public function getFirstEntities($query, $page, $limit)
    {
        $DQLParts = $query->getDQLParts();
        //Se obtienen los from del query
        $fr = $DQLParts['from'];
        $from = $fr[0];
        $alias = $from->getAlias();
        $query->select($alias);
        $query->setFirstResult($page);
        $query->setMaxResults($limit);
        $query->groupBy($alias.".id");
        
        return $query;
    }
    public function countTotal($query)
    {
        //Este es un comentario
        $DQLParts = $query->getDQLParts();
        //Se obtienen los from del query
        $fr = $DQLParts['from'];
        $from = $fr[0];
        $alias = $from->getAlias();
        $select = 'count('.$alias.'.id)';
        $query = $query->select($select)
                ->groupBy($alias.".id");
        $query = $query->getQuery();
        $query = $query->getArrayResult();
        return count($query);
    }
    public function getArrayEntityWithOneLevel(array $criteria, $orderBy = null, $page = null, $limit = null, array $filter = null, $entityName, $orderType = null ) {
        // echo "entre";
        try{
            $query = $this->getSQLStructure($criteria, $orderBy, $page, $limit, $filter, $entityName, null, $orderType);
        }
        catch(DBALException $e)
        {
            throw new \Exception("Error Processing Request", 1);
        }
        $count = null;
        if(!is_null($page) && !is_null($limit)){
            $count = $this->countTotal($query);
            $first = $this->getFirstEntities($query, $page, $limit);
            $ids = array();
            foreach($first->getQuery()->getArrayResult() as $value)
            {
                $ids[] = $value['id'];
            }
            try{
                $query = $this->getSQLStructure($criteria, $orderBy, $page, $limit, $filter, $entityName, $ids, $orderType);
            }
            catch(DBALException $e)
            {
                throw new \Exception("Error Processing Request", 1);
            }
        }
        $data = $query->getQuery()->getArrayResult();
        if(is_null($count)){
            $count = count($data);
        }
        return array(
            "total" => $count,
            "data" => $data
        );
    }
    public function getEntityWithOneLevel(array $criteria, array $orderBy = null, $sresult = null, $limit = null, $entityName) {
        $query = $this->getSQLStructure($criteria, $orderBy, $sresult, $limit, $entityName);
        $data = $query->getQuery()->getResult();
        $count = count($data);
        if(!is_null($sresult) && !is_null($limit))
        {
            $this->applyPagination($data, $count, $sresult, $limit);
        }
        return array(
            "total" => $count,
            "data" => $data
        );
    }
    public function applyPagination(&$data, $count, $start, $limit)
    {
        $paginateData = array();
        for($i=$start;$i<($start+$limit);$i++)
        {
            if($count > $i)
            {
                $paginateData[] = $data[$i];
            }
        }
        $data = $paginateData;
        return $data;
    }
    /**
     * Aplica un filtro a un query
     *
     * @param query - Query al que se le aplicará el filtro
     * @param filter - FIltro a aplciar
     * @param counter- contador 
     */
    public function setFilterToQuery(&$query, $filter, $counter)
    {
        //Se obtienen las DQLParts del query en forma de array
        $DQLParts = $query->getDQLParts();
        //Se obtienen los from del query
        $fr = $DQLParts['from'];
        $from = $fr[0];
        $from2 = $from->getFrom();
        //echo $from2;
        $alias = $from->getAlias();
        //Obtiene los Join del query
        $joins = $DQLParts['join'];
        if(count($joins) > 0){
            $join = $joins[$alias];
        }else{
            $join = array();
        }
        //Obtiene la metadata de la clase a la que se le aplica el filtro
        $ClassMetadata = $this->em->getClassMetadata($from2);
        $fieldMappings = $ClassMetadata->fieldMappings;
        $associationMappings = $ClassMetadata->associationMappings;
        //Obtiene el ultimo alias usado en el query
        $lastAlias = $alias;
        $lastProperty = null;
        $md = $ClassMetadata;
        $fm = $fieldMappings;
        $am = $associationMappings;
        //Se crea una variable que servirá de contador
        $i = 1;
        //Se obtienen los valores del filtro que llega por parametro
        $property = $filter['property'];
        $val = $filter['value'];
        if(is_null($val) || $val == "")
        {
            return $query;
        }
        $operator = trim($filter['operator']);
        $explode = explode(".", $property);
        //Se verifica si la propiedad que llega en el filtro es una propiedad de la clase, o es una relacion con otra entidad
        if(count($explode) > 1)
        {
            $lastPosition = null;
            //se recorre la variable explode que tiene los diferentes atributos siendo relaciones
            foreach($explode as $exp)
            {
                //se verifica si la propieda a evalutar es una asociacion
                // echo $exp;
                // print_r($am);
                $md = $this->isAssociation($exp, $am);
                if(!is_null($md))
                 {
                    $fm = $md->fieldMappings;
                    $am = $md->associationMappings;
                }
                //Se verifica si la propiedad a evaluar esta en los join del query
//                 echo "exp - ";
//                 print_r($exp);
//                 echo "join - ";
//                 print_r($join);
//                 echo "lastAlias - ";
//                 print_r($lastAlias);
                $jo = $this->isJoin($exp, $join, $lastAlias);
                if(!$jo && !is_null($md))
                {
                    //Se crea un nuevo Join en caso de que la metadata de la clase a la que se le aplico el query no este vacia y la variable del explode no sea un join
                    $query->leftJoin($lastAlias.".".$exp, $lastAlias.$i);
                    //Se guarda el nuevo ultimo alias
                    $lastAlias = $lastAlias.$i;
                    $i++;
                }
                $lastPosition = $exp;
            }
            //verifica si el lastPosition estaq en los fieldMapping
            if(array_key_exists($lastPosition, $fm))
            {
                $lastProperty = $lastPosition;
            }
            else
            {
                throw new \Exception("Propiedad $lastPosition no pertenece a la entidad");
            }
        }
        else
        {
            if(array_key_exists($explode[0], $fm))
            {
                $lastProperty = $explode[0];
            }
            else
            {
                throw new \Exception("Propiedad $explode[0] no pertenece a la entidad");
            }
        }
        //Obtiene el type de la propiedad a aaplicar el filtro
        $fmType = $fm[$lastProperty]['type'];
        $t = null;
        switch($fmType)
        {
            case "array":
                $t = \Doctrine\DBAL\Types\Type::TARRAY;
                break;
            case "simple_array":
                $t = \Doctrine\DBAL\Types\Type::SIMPLE_ARRAY;
                break;
            case "json_array":
                $t = \Doctrine\DBAL\Types\Type::JSON_ARRAY;
                break;
            case "bigint":
                $t = \Doctrine\DBAL\Types\Type::BIGINT;
                break;
            case "boolean":
                $t = \Doctrine\DBAL\Types\Type::BOOLEAN;
                break;
            case "datetime":
                $t = \Doctrine\DBAL\Types\Type::DATETIME;
                break;
            case "datetimetz":
                $t = \Doctrine\DBAL\Types\Type::DATETIMETZ;
                break;
            case "date":
                $t = \Doctrine\DBAL\Types\Type::DATE;
                break;
            case "time":
                $t = \Doctrine\DBAL\Types\Type::TIME;
                break;
            case "decimal":
                $t = \Doctrine\DBAL\Types\Type::DECIMAL;
                break;
            case "integer":
                $t = \Doctrine\DBAL\Types\Type::INTEGER;
                break;
            case "object":
                $t = \Doctrine\DBAL\Types\Type::OBJECT;
                break;
            case "smallint":
                $t = \Doctrine\DBAL\Types\Type::SMALLINT;
                break;
            case "string":
                $t = \Doctrine\DBAL\Types\Type::STRING;
                break;
            case "text":
                $t = \Doctrine\DBAL\Types\Type::TEXT;
                break;
            case "blob":
                $t = \Doctrine\DBAL\Types\Type::BLOG;
                break;
            case "float":
                $t = \Doctrine\DBAL\Types\Type::FLOAT;
                break;
            case "uuid":
                $t = \Doctrine\DBAL\Types\Type::GUID;
                break;
        }
        //En caso de que el tipo sea datetime
        if($fmType == "datetime")
        {
            if(is_array($val))
            {
                $first = new \DateTime($val[0]);
                $second = new \DateTime($val[1]." 23:59:59");
            }
            else
            {
                if($operator == "<=" || $operator == ">")
                {
                    $val = new \DateTime($val." 23:59:59");
                }
                elseif($operator == "=")
                {
                    $val1 = new \DateTime($val);
                    $val2 = new \DateTime($val." 23:59.59");
                }
                else
                {
                    $val = new \DateTime($val);
                }
            }
        }
        elseif($fmType == "date")
        {
            if(is_array($val))
            {
                $first = new \DateTime($val[0]);
                $second = new \DateTime($val[1]);
            }
            else
            {
                $val = new \DateTime($val);
            }
        }
        elseif($fmType == "bigint" || $fmType == "boolean" || $fmType == "decimal" || $fmType == "integer" || $fmType == "smallint" || $fmType == "float")
        {
            if($fmType == "boolean"){
                if($val == "false")
                {
                    $val = 0;
                }
                elseif($val == "true"){
                    $val = 1;
                }
            }
            if(!is_numeric($val))
            {
                return null;
            }
        }
        //añade where segun el operador que se envia
        switch($operator)
        {
            case "=":
                if($fmType == "datetime" || $fmType == "date")
                {
                    $query->andWhere($lastAlias.".".$lastProperty." BETWEEN :s".$counter." AND :p".$counter);
                    $query->setParameter('s'.$counter, $val1, $t);
                    $query->setParameter('p'.$counter, $val2, $t);
                }
                else
                {
                    $query->andWhere($lastAlias.".".$lastProperty." = :s".$counter);
                    $query->setParameter('s'.$counter, $val, $t);
                }
                break;
            case ">":
                $query->andWhere($lastAlias.".".$lastProperty." > :s".$counter);
                $query->setParameter('s'.$counter, $val, $t);
                break;
            case "<":
                $query->andWhere($lastAlias.".".$lastProperty." < :s".$counter);
                $query->setParameter('s'.$counter, $val, $t);
                break;
            case ">=":
                $query->andWhere($lastAlias.".".$lastProperty." >= :s".$counter);
                $query->setParameter('s'.$counter, $val, $t);
                break;
            case "<=":
                $query->andWhere($lastAlias.".".$lastProperty." <= :s".$counter);
                $query->setParameter('s'.$counter, $val, $t);
                break;
            case "has":
                $val = "%".strtoupper($val)."%";
                $query->andWhere("upper(".$lastAlias.".".$lastProperty.") LIKE :s".$counter);
                $query->setParameter('s'.$counter, $val, $t);
                break;
            case "equal":
                $query->andWhere($lastAlias.".".$lastProperty." = :s".$counter);
                $query->setParameter('s'.$counter, $val, $t);
                break;
            case "start_with":
                // $val = $val."%";
                $val = strtoupper($val)."%";
                $query->andWhere("upper(".$lastAlias.".".$lastProperty.") LIKE :s".$counter);
                $query->setParameter('s'.$counter, $val, $t);
                break;
            case "end_with":
                // $val = "%".$val;
                $val = "%".strtoupper($val);
                $query->andWhere("upper(".$lastAlias.".".$lastProperty.") LIKE :s".$counter);
                $query->setParameter('s'.$counter, $val, $t);
                break;
            case "between":
                $query->andWhere($lastAlias.".".$lastProperty." BETWEEN :s".$counter." AND :p".$counter);
                $query->setParameter('s'.$counter, $first, $t);
                $query->setParameter('p'.$counter, $second, $t);
                break;
            case "empty":
                $query->andWhere($lastAlias.".".$lastProperty." IS NULL");
                break;
        }
        return $query;
    }
    /**
     * Funcion que verifica si un atributo de una entidad es una asociacion
     * 
     * @param explode - atributo de la entidad a evaluar
     * @param association - array con las asociaciones de la entidad
     * @return md - Metadata de la entidad asociacion
     */
    private function isAssociation($explode, $association)
    {
        if(array_key_exists($explode, $association))
        {
            $entityName = $association[$explode]['targetEntity'];
            $md = $this->em->getClassMetadata($entityName);
            return $md;
        }
        return null;
    }
    /**
     * Funcion que verifica si un atributo de una entidad que es relacion, ya es un join del query
     * 
     * @param explode - Atributo a verificar si es un join en el query
     * @param join - array con todos los join del query
     * @param lastAlias - Ultimo alias usado en el query
     * @return true - en caso de que si sea un join del query
     * @return false - en caso de que no sea un join del query
     */
    private function isJoin($explode , $join, &$lastAlias)
    {
        foreach($join as $item)
        {
            if(strcmp($item->getJoin(), $lastAlias.".".$explode) == 0)
            {
                $lastAlias = $item->getAlias();
                return true;
            }
        }
        return false;
    }
}