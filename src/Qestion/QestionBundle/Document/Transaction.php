<?php
namespace Qestion\QestionBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\Document(repositoryClass="Qestion\QestionBundle\Repository\TransactionRepository")
 */
class Transaction
{
	/**
	 * @MongoDB\Id
	 */
	private $id;

	/**
	 * @MongoDB\Date
     * @Assert\DateTime()
	 */
	private $insertDate;

	/**
	 * @MongoDB\String
	 */
	private $entity;

    /**
     * @MongoDB\EmbedMany(targetDocument="ValueChange")
     */
	private $valueChange;

	/**
     * @MongoDB\EmbedOne(targetDocument="User")
     */
	private $user;

	/**
	 * @MongoDB\String
	 */
	private $action;

    /**
     * @MongoDB\String
     */
    private $entityId;
    public function __construct()
    {
        $this->valueChange = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set insertDate
     *
     * @param date $insertDate
     * @return self
     */
    public function setInsertDate($insertDate)
    {
        $this->insertDate = $insertDate;
        return $this;
    }

    /**
     * Get insertDate
     *
     * @return date $insertDate
     */
    public function getInsertDate()
    {
        return $this->insertDate;
    }

    /**
     * Set entity
     *
     * @param string $entity
     * @return self
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
        return $this;
    }

    /**
     * Get entity
     *
     * @return string $entity
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Add valueChange
     *
     * @param Qestion\QestionBundle\Document\ValueChange $valueChange
     */
    public function addValueChange(\Qestion\QestionBundle\Document\ValueChange $valueChange)
    {
        $this->valueChange[] = $valueChange;
    }

    /**
     * Remove valueChange
     *
     * @param Qestion\QestionBundle\Document\ValueChange $valueChange
     */
    public function removeValueChange(\Qestion\QestionBundle\Document\ValueChange $valueChange)
    {
        $this->valueChange->removeElement($valueChange);
    }

    /**
     * Get valueChange
     *
     * @return \Doctrine\Common\Collections\Collection $valueChange
     */
    public function getValueChange()
    {
        return $this->valueChange;
    }

    /**
     * Set user
     *
     * @param Qestion\QestionBundle\Document\User $user
     * @return self
     */
    public function setUser(\Qestion\QestionBundle\Document\User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return Qestion\QestionBundle\Document\User $user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set action
     *
     * @param string $action
     * @return self
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * Get action
     *
     * @return string $action
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set entityId
     *
     * @param string $entityId
     * @return self
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;
        return $this;
    }

    /**
     * Get entityId
     *
     * @return string $entityId
     */
    public function getEntityId()
    {
        return $this->entityId;
    }
}
