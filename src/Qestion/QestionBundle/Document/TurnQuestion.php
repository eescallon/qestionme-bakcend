<?php
namespace Qestion\QestionBundle\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @MongoDB\Document
 */
class TurnQuestion
{
    /**
     * @MongoDB\Id  
     */
    private $id;
    
    /** @MongoDB\String */
    private $gameId;

    /** @MongoDB\String */
    private $playerId;

    /** @MongoDB\String */
    private $questionId;

    /** @MongoDB\String */
    private $answerId;

    /** @MongoDB\Boolean */
    private $right;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set gameId
     *
     * @param string $gameId
     * @return self
     */
    public function setGameId($gameId)
    {
        $this->gameId = $gameId;
        return $this;
    }

    /**
     * Get gameId
     *
     * @return string $gameId
     */
    public function getGameId()
    {
        return $this->gameId;
    }

    /**
     * Set playerId
     *
     * @param string $playerId
     * @return self
     */
    public function setPlayerId($playerId)
    {
        $this->playerId = $playerId;
        return $this;
    }

    /**
     * Get playerId
     *
     * @return string $playerId
     */
    public function getPlayerId()
    {
        return $this->playerId;
    }

    /**
     * Set questionId
     *
     * @param string $questionId
     * @return self
     */
    public function setQuestionId($questionId)
    {
        $this->questionId = $questionId;
        return $this;
    }

    /**
     * Get questionId
     *
     * @return string $questionId
     */
    public function getQuestionId()
    {
        return $this->questionId;
    }

    /**
     * Set answerId
     *
     * @param string $answerId
     * @return self
     */
    public function setAnswerId($answerId)
    {
        $this->answerId = $answerId;
        return $this;
    }

    /**
     * Get answerId
     *
     * @return string $answerId
     */
    public function getAnswerId()
    {
        return $this->answerId;
    }

    /**
     * Set right
     *
     * @param boolean $right
     * @return self
     */
    public function setRight($right)
    {
        $this->right = $right;
        return $this;
    }

    /**
     * Get right
     *
     * @return boolean $right
     */
    public function getRight()
    {
        return $this->right;
    }
}
